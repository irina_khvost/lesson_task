package Lesson12_Enam;

public class DateOfMeet {

    private String meet;
    private String meetDes;
    private Month month;
    private Week week;
    private String hh;
    private String min;
    private int number;


    public DateOfMeet() {
        this.meet = "";
        this.meetDes = "";
        this.month = null;
        this.week = null;
        this.number = 0;
        this.hh = "";
        this.min = "";


    }

    public DateOfMeet(String meet, String meetDes, Month month, Week week, int number,String hh, String min) {
        this.meet = meet;
        this.meetDes = meetDes;
        this.month = month;
        this.week = week;
        this.hh = hh;
        this.min = min;
        this.number = number;
    }

    public String getMeet() {
        return meet;
    }

    public void setMeet(String meet) {
        this.meet = meet;
    }

    public String getMeetDes() {
        return meetDes;
    }

    public void setMeetDes(String meetDes) {
        this.meetDes = meetDes;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public Week getWeek() {
        return week;
    }

    public void setWeek(Week day) {
        this.week = day;
    }

    public String getHh() {
        return hh;
    }

    public void setHh(String hh) {
        this.hh = hh;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    String show() {
        return "Встреча: " + getMeet() + "( " + getMeetDes() + ")\n" +
                "Дата: " + getNumber()+ " " + getWeek() +" " + getMonth() + " в " + getHh() + ":" + getMin();


    }
}
