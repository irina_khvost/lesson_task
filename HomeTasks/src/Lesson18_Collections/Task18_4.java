package Lesson18_Collections;
/*
Создать arraylist типа string произвольной длины. Добавлять в него строки.
Но не в конец, а в начало списка.
 */

import java.util.ArrayList;

public class Task18_4 {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            arrayList.add(0,"arrayList"+i);
        }

        for (String s : arrayList) {
            System.out.println(s);

        }
    }
}
