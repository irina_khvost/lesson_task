package Lesson18_Collections;
/*
Создать arraylist типа string и заполнить его 10 строками. Вывести на экран
содержимое списка.
 */

import java.util.ArrayList;

public class Task18_1 {
    public static void main(String[] args) {
        ArrayList<String> arrayStr = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arrayStr.add("array" + i);
        }
        for (String s : arrayStr) {
            System.out.print(s + " ");
        }
    }
}
