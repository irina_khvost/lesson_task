package Lesson18_Collections;

import java.util.ArrayList;

/*
Создать arraylist типа string и заполнить его 10 строками. Произвести
циклический сдвиг вправо 17 раз и вывести на экран.
 */
public class Task18_5 {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        ArrayList<String> arrayTmp = new ArrayList<>();


        for (int i = 0; i < 10; i++) {
            arrayList.add("array" + i);
            arrayTmp.add("");
        }

        int a = 17 % arrayList.size();
        for (int i = 0; i < arrayList.size(); i++) {
            if ((i + a) >= arrayTmp.size())
                a = a - arrayTmp.size();
            arrayTmp.set(i + a, arrayList.get(i));
        }

        for (String s : arrayTmp) {
            System.out.println(s);
        }
    }
}
