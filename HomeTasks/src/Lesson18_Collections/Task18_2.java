package Lesson18_Collections;
/*
Создать arraylist типа string и заполнить его 10 строками с клавиатуры. С
помощью цикла найти строку максимально длины(или несколько, если таковые
имеются) и вывести её(их) на экран
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Task18_2 {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter text: ");
        for (int i = 0; i < 10; i++) {
            arrayList.add(sc.nextLine());
        }

        String str = arrayList.get(0);
        for (int i = 1; i < arrayList.size()-1; i++) {
            if (str.length() < arrayList.get(i).length()) {
                str = arrayList.get(i);
            }
        }

        for (String s : arrayList) {
            if (s.length() == str.length())
                System.out.println(s);
        }
    }
}
