package Lesson26_ReadWrite;
/**
 * 12 графов и графинь танцуют вальс. Каждый такт они перемещаются по танцевальному залу.
 * Нужно создать классы граф и графиня в каждом из которых будет поля “имя” и их координаты (х и у).
 * Каждый такт танца они будут сдвигаться в случайном направлении на единицу.
 * После каждого “шага” нужно записывать их новое положение в bin-файл(удаляя при этом старое).
 * В программе должно быть два режима. Первый это такты танцев, который будет каждый раз спрашивать “еще такт?”
 * до тех пор пока пользователь не скажет остановиться. И второй режим, который считывает bin-файл
 * и показывает на экране координаты и имена каждого из танцующих.
 */

import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // размер поля для танцев
        int n = 25;
        ArrayList<Human> humans = createDanserc(n);
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Хотите танцевать (0 - нет/ 1 - да): ");
            int s = sc.nextInt();
            if (s == 1) {
                for (Human human : humans) {
                    dance(human, n);
                }
                writeFile(humans);
                readFile().forEach(System.out::println);
            } else break;
        }
    }

    private static ArrayList<Human> createDanserc(int n) {
        ArrayList<Human> dancers = new ArrayList<>();
        Random rnd = new Random();
        //количество графов и графинь
        int count = 12;
        for (int i = 0; i < count; i++) {
            dancers.add(new Graph("Count" + i, new Point(rnd.nextInt(n), rnd.nextInt(n))));
            dancers.add(new Countess("Countess" + i, new Point(rnd.nextInt(n), rnd.nextInt(n))));
        }
        return dancers;
    }

    private static void dance(Human human, int n) {
        Random rnd = new Random();
        Step[] step = Step.values();
        int x = (int) human.getPoint().getX();
        int y = (int) human.getPoint().getY();
        boolean flag = true;
        while (flag) {
            int a = rnd.nextInt(step.length);
            switch (a) {
                case 0:
                    human.setPoint(new Point(x, y - 1)); //LEFT
                    break;
                case 1:
                    human.setPoint(new Point(x, y + 1)); //RIGHT
                    break;
                case 2:
                    human.setPoint(new Point(x - 1, y)); //UP
                    break;
                case 3:
                    human.setPoint(new Point(x + 1, y)); //DOWN
                    break;
            }
            if (human.getPoint().getX() <= n || human.getPoint().getY() <= n)
                flag = false;
        }
    }

   /* private static void readFile() {
        String path = "C:\\Users\\Sacinandan Kisor das\\IdeaProjects\\HomeTasks\\src\\Lesson26_ReadWrite\\step.bin";
        try (DataInputStream dis = new DataInputStream(new FileInputStream(path))) {
            while (true) {
                String name = dis.readUTF();
                int x = dis.readInt();
                int y = dis.readInt();
                System.out.printf("Name: %10s \tX: %4s \tY: %4s \n", name, x, y);
            }
        } catch (EOFException ex) {
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }*/

    private static ArrayList<String> readFile() {
        String path = "C:\\Users\\Sacinandan Kisor das\\IdeaProjects\\HomeTasks\\src\\Lesson26_ReadWrite\\step.txt";
        ArrayList<String> humans = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(path);
             BufferedInputStream bis = new BufferedInputStream(fis)) {
            int c;
            String str = "";
            while ((c = bis.read()) != -1) {
                str = str + (char) c;
            }
            humans.addAll(Arrays.asList(str.split("\r\n")));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return humans;
    }

    /*private static void writeFile(ArrayList<Human> humans) {
        String path = "C:\\Users\\Sacinandan Kisor das\\IdeaProjects\\HomeTasks\\src\\Lesson26_ReadWrite\\step.bin";
        try (DataOutputStream dos = new DataOutputStream(new FileOutputStream(path))) {
            for (Human human : humans) {
                dos.writeUTF(human.getName());
                dos.writeInt(human.getPoint().x);
                dos.writeInt(human.getPoint().y);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }*/

    private static void writeFile(ArrayList<Human> humans) {
        String path = "C:/Users/Sacinandan Kisor das/IdeaProjects/HomeTasks/src/Lesson26_ReadWrite/step.txt";
        try (FileOutputStream fos = new FileOutputStream(path);
             BufferedOutputStream bos = new BufferedOutputStream(fos)) {
            for (Human human : humans) {
                bos.write(human.toString().getBytes());
                bos.write("\r\n".getBytes());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
