package Lesson26_ReadWrite;

import java.awt.*;

public abstract class Human {
    private String name;
    private Point point;

    public Human(String name, Point point) {
        this.name = name;
        this.point = point;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return String.format("Name: %10s \tX: %4s \tY: %4s", name, point.getX(), point.getY());
    }
}
