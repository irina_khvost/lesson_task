package Lesson28_Threads.Task3;

class Image {
    private Pixel[] data;
    private int size;
    private int countToLoad;

    Image(int size) {
        countToLoad = 0;
        this.size = size;
        data = new Pixel[size];
        for (int i = 0; i < data.length; i++) {
            data[i] = new Pixel(i);
        }
    }

    synchronized boolean canGet() {
        if (countToLoad == size)
            return false;
        return true;
    }

    void print() {
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i].val + "_");
        }
        System.out.println();
    }

    synchronized Pixel getData() {
        Pixel res = data[countToLoad];
        countToLoad++;
        return res;
    }

    synchronized void setData(Pixel pixel) {
        data[pixel.pos] = pixel;
    }

}
