package Lesson28_Threads.Task3;

import java.util.Random;

class MyThread implements Runnable {
    private Image image;
    private Random random;

    MyThread(Image image) {
        this.image = image;
        random = new Random();
    }

    @Override
    public void run() {
        Pixel tmp;
        while (image.canGet()) {
            tmp = image.getData();
            System.out.println(Thread.currentThread().getName() + ": " + tmp.pos + " " + tmp.val);
            try {
                Thread.sleep((random.nextInt(3) + 1) * 100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            tmp.val = random.nextInt(9) + 1;
            System.out.println(Thread.currentThread().getName() + ": new data" + tmp.pos + " " + tmp.val);
            image.setData(tmp);
        }
    }
}
