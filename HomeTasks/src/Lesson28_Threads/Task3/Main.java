package Lesson28_Threads.Task3;

/**
 *Нужно в многопоточном режиме обработать изображение. Для этого придётся
 * создать “общедоступный” для всех потоков класс “Изображение”, состоящее из класса
 * “пиксель”(который, кстати, тоже нужно создать), изначально заполненных нулями. Помимо
 * них нужен класс отвечающий за “обработку” изображения. “Обработать” означает, что все
 * нули заменяются на случайное значение от 1 до 9.
 * */

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Image image = new Image(20);
        image.print();

        Thread thread1 = new Thread(new MyThread(image));
        Thread thread2 = new Thread(new MyThread(image));
        Thread thread3 = new Thread(new MyThread(image));

        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();

        image.print();
    }
}


