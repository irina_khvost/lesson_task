package Lesson28_Threads.Task3;

import java.util.Random;

class ImageV1 implements Runnable {
    Random rnd = new Random();
    volatile int[] arr = new int[50];

    void write() {
        while (!Thread.currentThread().isInterrupted())
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == 0) {
                    arr[i] = rnd.nextInt(9) + 1;
                    System.out.println(Thread.currentThread().getName());
                    break;
                }
                if (i == arr.length - 1 && arr[i] != 0) {
                    Thread.currentThread().interrupt();
                    System.out.println(Thread.currentThread().getName() + " stopped.");
                }
            }
    }

    @Override
    public void run() {
        write();
    }

    void print() {
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }
}
