package Lesson28_Threads.Task2;

public class Main {
    public static void main(String[] args) {

        Image image = new Image();
        Thread first = new Thread(image);
        Thread second = new Thread(image);
        first.start();
        second.start();

        try {
            first.join();
            second.join();
            image.print();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }


}
