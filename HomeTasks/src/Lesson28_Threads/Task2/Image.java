package Lesson28_Threads.Task2;

import java.util.Random;

class Image extends Main implements Runnable {
    private Random rnd = new Random();
    volatile int[] pixel = new int[20];


    synchronized void write() {
        while (!Thread.currentThread().isInterrupted()) {
            for (int i = 0; i < pixel.length; i++) {
                if (pixel[i] == 0) {
                    pixel[i] = rnd.nextInt(9) + 1;
                    System.out.println(Thread.currentThread().getName() + " number:" + i + " value:" + pixel[i]);
                    break;
                }
                if (i == pixel.length - 1 && pixel[i] != 0) {
                    Thread.currentThread().interrupt();
                    print();
                    System.out.println("Stop: " + Thread.currentThread().getName());
                }
            }
        }
    }

    void print() {
        for (int i : pixel) {
            System.out.print(i + " ");
        }
    }


    @Override
    public void run() {
        write();
    }
}
