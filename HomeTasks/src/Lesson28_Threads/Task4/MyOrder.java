package Lesson28_Threads.Task4;

class MyOrder {
    int id = 0;
    //    static volatile int id = 0;
    private int time;

    public MyOrder(int time, int id) {
        this.time = time;
//        id++;
        this.id = id;
    }

    synchronized public int getId() {
        return id;
    }

    synchronized public int getTime() {
        return time;
    }
}
