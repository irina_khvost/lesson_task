package Lesson28_Threads.Task4;

import java.util.ArrayList;

class Order {

    private ArrayList<MyOrder> orderList;
    int id=0;

    synchronized int getID(){
        return id++;
    }


    public Order() {
        orderList = new ArrayList<>();
    }

    synchronized MyOrder getOrder() {
        if (orderList.size() != 0) {
            MyOrder res = orderList.get(orderList.size() - 1);
            orderList.remove(orderList.size() - 1);
            return res;
        }
        return null;
    }

    synchronized void setOrder(MyOrder order) {
        orderList.add(0, order);
    }

    Integer sizeList() {
        return orderList.size();
    }
}
