package Lesson28_Threads.Task4;

class Cook extends Thread {
    Order order;
    private boolean isActive;

    Cook(String name, Order order) {
        super(name);
        this.order = order;
        isActive = true;
    }

    void disable(){
        isActive = false;
    }

    @Override
    public void run() {
        while (isActive) {
            if (order.sizeList() > 0) {
                try {
                    MyOrder tmp = order.getOrder();
                    int time = tmp.getTime();
                    Thread.sleep(time);
                    System.out.println(Thread.currentThread().getName() + " выполнял заказ " + tmp.getId());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else System.out.println("Жду заказ " + Thread.currentThread().getName());
        }
        interrupt();
    }
}
