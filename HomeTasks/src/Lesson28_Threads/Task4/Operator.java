package Lesson28_Threads.Task4;

import java.util.Random;

class Operator extends Thread {
    private Random rnd = new Random();
    private boolean isActive;
    Order order;

    void disable() {
        isActive = false;
    }

    Operator(String name, Order order) {
        super(name);
        this.order = order;
        isActive = true;
    }

    @Override
    public void run() {
        while (isActive) {
            if (order.sizeList() < 6) {
                int time = (rnd.nextInt(9) + 1) * 100;
                MyOrder tmp = new MyOrder(time,order.getID());
                order.setOrder(tmp);
                System.out.println("Оператор: " + Thread.currentThread().getName() + ", принял заказ на " + tmp.getId());
                try {
                    Thread.sleep((rnd.nextInt(5) + 1) * 100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Готов к принятию заказа " + Thread.currentThread().getName());
            } else {
                try {
                    System.out.println("Оператор: " + Thread.currentThread().getName() + " стол заказов переполнен.");
                    Thread.sleep((rnd.nextInt(3) + 1) * 100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        interrupt();
    }
}
