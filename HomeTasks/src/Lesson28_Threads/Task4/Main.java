package Lesson28_Threads.Task4;

/**
 * Есть определенное количество потоков, которые генерируют "задачу" (число от 1 до 10).
 * А есть другое определенное количество потоков, которые при появлении "задачи", сразу
 * забирают ее на выполнение. А выполнение заключается в том что бы уснуть на заданное число.
 */

public class Main {
    public static void main(String[] args) {
        Order order = new Order();
        Operator operator1 = new Operator("Operator1", order);
        Operator operator2 = new Operator("Operator2", order);
        Operator operator3 = new Operator("Operator3", order);

        operator1.start();
        operator2.start();
        operator3.start();

        Cook cook1 = new Cook("Cook1",order);
        Cook cook2 = new Cook("Cook2",order);

        cook1.start();
        cook2.start();

        try {
            Thread.sleep(5000);
            operator1.disable();
            operator2.disable();
            operator3.disable();
            Thread.sleep(500);
            cook1.disable();
            cook2.disable();
//            Thread.sleep(1000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            cook1.join();
            cook2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Рабочий день закончен.");






    }
}
