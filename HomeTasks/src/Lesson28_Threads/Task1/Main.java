package Lesson28_Threads.Task1;

/**
 * Разведчики обнаружили неподалеку рудник! Ты должен, как можно скорее,
 * добыть оттуда всё золото! Держи пять рабочих и вперёд!
 * Нужно создать класс “Рабочий”, который будет являться потоком. Помимо этого нужны
 * два класса “Шахта” и “Замок”, которые будут являться “общественным” ресурсом для
 * рабочих. Рабочий будет заходить в шахту, “засыпать” на случайное время, выходить,
 * заходить в замок, “засыпать” на случайное время. В шахту и замок заходить можно только по одному рабочему.
 */

public class Main {
    public static void main(String[] args) {
        Mine mine = new Mine();
        Castle castle = new Castle();
        Thread first = new Thread(new Workers(mine, castle));
        Thread second = new Thread(new Workers(mine, castle));
        Thread third = new Thread(new Workers(mine, castle));
        first.start();
        second.start();
        third.start();



    }
}
