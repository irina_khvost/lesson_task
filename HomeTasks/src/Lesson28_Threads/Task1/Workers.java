package Lesson28_Threads.Task1;

import java.util.Random;

class Workers implements Runnable {
    private Mine mine;
    private Castle castle;
    private Random random;
    private int gold;

    Workers(Mine mine, Castle castle) {
        this.mine = mine;
        random = new Random();
        gold = 0;
        this.castle = castle;
    }

    @Override
    public void run() {
        while (!mine.isEmpty()) {
            System.out.println("In mine now: " + mine.gold);
            System.out.println(Thread.currentThread().getName() +
                    ": I go into mine!");
            int myGold = mine.getGold();
            gold += myGold;
            System.out.println(Thread.currentThread().getName() +
                    ": I have: " + gold);
            try {
                Thread.sleep((random.nextInt(3) + 1) * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() +
                    ": Returned from mine!");
            castle.setGold(castle.getGold() + myGold);
            System.out.println("In castle golds: " + castle.getGold());
        }
    }
}
