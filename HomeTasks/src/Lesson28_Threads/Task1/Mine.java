package Lesson28_Threads.Task1;

class Mine {
    int gold;

    Mine() {
        gold = 1000;
    }

    synchronized int getGold() {
        if (!isEmpty()) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            //System.out.println("In mine now: " + gold);
            gold -= 50;
            return 50;
        } else return 0;
    }

    synchronized boolean isEmpty() {
        if (gold > 0)
            return false;
        return true;
    }
}
