package Lesson28_Threads.Task1;

class Castle {
    private int gold;

    synchronized int getGold() {
        return gold;
    }

    synchronized void setGold(int gold) {
        this.gold = gold;
    }
}
