package Lesson22_HashMap;

import java.util.ArrayList;

public class Hash<K, V>  {
    ArrayList<K> keys = new ArrayList<>();
    ArrayList<ArrayList<V>> vals = new ArrayList<>();

    //добавление элемента по ключу
    ArrayList<V> put(K key, V value) {
        if (keys.contains(key)) {
            vals.get(keys.indexOf(key)).add(value);
        } else {
            keys.add(key);
            ArrayList<V> tmp = new ArrayList<>();
            tmp.add(value);
            vals.add(tmp);
            //vals.add(new ArrayList<V>() {{add(value);}});
        }
        return vals.get(keys.indexOf(key));
    }

    //поиск по ключу
    ArrayList<V> containsKey(K key) {
        if (keys.contains(key))
            return vals.get(keys.indexOf(key));
        else return null;

    }

    //поиск по значению
/*
    ArrayList<K> containsValue(V value) {
        if (vals.contains(value)) {
            return keys.get(vals.indexOf(value));
        } else return null;

    }
*/





    ArrayList<V> getV(K key) {
        return vals.get(keys.indexOf(key));
    }

    ArrayList<K> getKeys() {
        return keys;
    }



}
