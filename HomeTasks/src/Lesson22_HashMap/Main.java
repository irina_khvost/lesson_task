package Lesson22_HashMap;

import java.util.HashMap;

/**
 * Создать класс, который расширяет возможности hashMap. А именно, позволяет
 * хранить под одним ключом несколько значений. Должны быть реализованы функции
 * добавления элемента по ключу, поиск по ключу, поиск по значению. Изменение
 * значения по ключу. Удаление элементов по значению.
 */

public class Main {
    public static void main(String[] args) {
        HashMap<Integer, String> a = new HashMap<>();

        Hash<Integer, String> hash = new Hash<>();
        hash.put(5,"a");
        hash.put(5,"b");
        hash.put(3,"c");
        hash.put(3,"c");
        hash.put(3,"c");
        hash.put(3,"c");
        hash.put(1,"m");
        System.out.println(hash.containsKey(1));

        for (Integer key : hash.getKeys()) {
            for (String s : hash.getV(key)) {
                System.out.print(s + " ");
            }
            System.out.println();
        }
    }
}
