package Lesson20_Training;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Два нечетных простых числа, отличающиеся на 2, называются близнецами. Например, числа 5 и 7.
 * Напишите программу, которая будет находить все числа-близнецы на отрезке [2; 1000].
 */

public class TwinNumbers {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите конечное число интервала для поиска чисел близнецов: ");
        try {
            int n = sc.nextInt();
            searchTwin(primeNumber(n));
        }catch (Exception e) {
            System.out.println("Error");
        }
    }

    //заполнение листа простыми числами
    static ArrayList<Integer> primeNumber(int n) {
        ArrayList<Integer> list = new ArrayList<>();
        //заполнение ArrayList до n
        for (int i = 2; i <= n; i++) {
            list.add(i);
        }

        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); ) {
                if (list.get(j) % list.get(i) != 0) {
                    j++;
                } else {
                    list.remove(j);
                }
            }
        }
        return list;
    }

    //Поиск чисел близнецов
    static void searchTwin(ArrayList<Integer> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i) - list.get(i + 1) == -2) {
                System.out.println(list.get(i) + " & " + list.get(i + 1));
            }
        }
    }
}
