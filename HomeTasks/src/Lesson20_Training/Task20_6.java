package Lesson20_Training;

import java.util.Scanner;

/*
Совершенным числом называется число, равное сумме своих делителей, меньших его
самого. Например, 28=1+2+4+7+14. Определите, является ли данное натуральное число
совершенным. Найдите все совершенные числа на данном отрезке (возможно, стоит применить идею
решета Эратосфена).
 */
public class Task20_6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите максимальную границу отрезка для поиска совершенного числа: ");
        int n = sc.nextInt();
        outloop:
        for (int i = 2; i <= n; i++) {
            int num;
            if (naturNum((int) Math.pow(2, i) - 1)) {
                num = (int) (Math.pow(2, i - 1)) * (int) (Math.pow(2, i) - 1);
                if (num < n)
                    System.out.println(num);
                else
                    break outloop;
            }
        }
        System.out.println("Вывод закончен.");
    }

    static boolean naturNum(int a) {
        boolean flag = false;
        int col = 2;
        double result = 0.0;
        for (int del = 2; del <= a; del++) {
            result = a % del;
            if (result == 0 && del != a) {
                col++;
            }
        }
        if (col < 3) {
            flag = true;
        }
        return flag;
    }
}
