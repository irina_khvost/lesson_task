package Lesson20_Training;

/**
 * Найдите количество и сумму цифр в данном натуральном числе.
 * Дано натуральное число. Поменяйте в нем порядок цифр на обратный. 
 */

import java.util.Scanner;

public class Amount_Sum_Revers_Number {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число : ");
        String s = sc.nextLine();
        if (tryParseInt(s)) {
            System.out.println("Количество цифр: " + s.length());
            System.out.println("Сумма цифр числа " + s + " равна: " + sumNumber(s));
            System.out.println("Обратное число числа " + s + " равна: " + reversNumber(s));

        } else {
            System.out.println("Error! Вы ввели не число!");
        }

    }

//Проверка на то что введено число
    static boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
//Подсчет суммы цифр числа
    static int sumNumber (String s) {
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            sum +=  Character.getNumericValue(s.charAt(i));
        }
        return sum;
    }
    
    static String reversNumber(String s) {
        String i = "";
        for (int j = s.length()-1; j >= 0; j--) {
            i = i + "" + Character.getNumericValue(s.charAt(j));
        }
        return i;
    }
}
