package Lesson20_Training;

import java.util.Scanner;

/**
 * Числа, запись которых состоит из двух одинаковых последовательностей цифр, называются симметричными.
 * Например, 357357 или 17421742. Определите, является ли данное натуральное число симметричным.
 */

public class SymmetricalNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число: ");
        String s = sc.nextLine();
        if (Amount_Sum_Revers_Number.tryParseInt(s)) {
            if (s.length() % 2 == 0) {
                if (s.substring(0, s.length() / 2).equals(s.substring(s.length() / 2))) {
                    System.out.println("Число симметричное!");
                }
                else System.out.println("Число не симметричное!");
            } else
                System.out.println("Число не симметричное!");

        } else {
            System.out.println("Error! Вы ввели не число!");
        }
    }
}

