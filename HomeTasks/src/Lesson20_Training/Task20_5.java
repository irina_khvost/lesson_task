package Lesson20_Training;
/*
Б. Кордемский указывает одно интересное число 145, которое равно сумме факториалов
своих цифр: 145=1!+4!+5!. Он пишет, что неизвестно, есть ли еще такие числа,
удовлетворяющие названному условию. Написать программу по нахождению таких чисел.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Task20_5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число максимум для поиска: ");
        int n = sc.nextInt();
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 1; i <= n; i++) {
            int k = i;
            int s = 0;
            while (k > 0) {
               s += factorial(k % 10);
               k = k / 10;
            }
            if (i == s) {
                list.add(i);
            }
        }
        for (Integer integer : list) {
            System.out.println(integer);
        }
    }

    static int factorial(int x) {
        if (x <= 1)
            return 1;
        return x * factorial(x - 1);
    }
}
