package Lesson20_Training.PythagoreanNumbers;

import java.util.ArrayList;

public class PythagoreanNumbers {

    private class Triangle {
        int a;
        int b;
        int c;

        Triangle(int a, int b, int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        @Override
        public String toString() {
            return a + " " + b + " " + c;
        }
    }

    public PythagoreanNumbers(int n) {
        allTriangles(n);
        mainTriangles();
    }

    ArrayList<Triangle> list = new ArrayList<>();

    //Заполнение списка Пифагоровыми тройками
    private void allTriangles(int n) {
        for (int b = 1; b < n; b++) {
            for (int a = 1; a < b; a++) {
                double c = Math.sqrt(a * a + b * b);
                if (c - (int) c == 0 && c <= n) {
                    list.add(new Triangle(a, b, (int) c));
                }
            }
        }
    }

    //Оставляем только основные тройки Pythagorean Triangles
    private void mainTriangles() {
        for (int i = 0; i < list.size(); ) {
            for (int j = 2; j < list.get(i).a; j++) {
                if ((list.get(i).a % j == 0) && (list.get(i).b % j == 0) && (list.get(i).c % j == 0)) {
                    list.remove(i);
                    i--;
                    break;
                }
            }
            i++;
        }
    }

    void show() {
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }

}



