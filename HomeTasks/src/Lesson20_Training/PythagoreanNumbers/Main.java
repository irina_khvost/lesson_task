package Lesson20_Training.PythagoreanNumbers;
/**
 * Пифагоровы числа. Три натуральных числа a, b и c образуют пифагорову тройку, если
 * c^2=a^2+b^2. Пифагорова тройка называется основной, если наибольший общий делитель ее чисел
 * равен единице. Например, 3, 4, 5 - основная тройка, 6, 8, 10 - производная тройка. Найдите все
 * основные пифагоровы тройки, числа в которых не превышают данное число max.
 */

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число максимум для поиска: ");
        int n = sc.nextInt();
        new PythagoreanNumbers(n).show();
    }
}
