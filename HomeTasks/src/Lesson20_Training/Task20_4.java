package Lesson20_Training;
/*
Однажды математик С. Улам разделил лист бумаги на клетки и, написав в центре 1, начал
писать по спирали против часовой стрелки все натуральные числа подряд, выделяя простые числа.
Скоро простые числа выстроились в довольно-таки закономерном порядке, образуя интересный узор.
Этот узор позже стал объектом исследования и получил название скатерть Улама.
Составьте программу, демонстрирующую скатерть Улама размером 100 х 100 клеток (вместо простых
чисел выводите звездочку "*").
 */

public class Task20_4 {
    public static void main(String[] args) {
        int[][] ulam = new int[100][100];
        int num = 1;
        int contur = 1;

        outofloop:
        for (int i = 49; i < ulam.length; ) {
            for (int j = 49; j < ulam[i].length; ) {
                if (i == j) {
                    ulam[i][j] = ++num;
                    j--;
                }
                if (i - j == 1) {
                    for (int k = 0; k < contur; k++) {
                        ulam[i][j] = ++num;
                        i++;
                    }
                    contur++;
                }
                if (i - j == contur) {
                    for (int k = 0; k < contur; k++) {
                        ulam[i][j] = ++num;
                        j++;
                    }
                }
                if (i == j) {
                    for (int k = 0; k < contur; k++) {
                        ulam[i][j] = ++num;
                        i--;
                    }
                }
                if (j - i == contur) {
                    if (i != 0 && j != ulam.length - 2) {
                        contur++;
                    }
                    for (int k = 0; k < contur; k++) {
                        ulam[i][j] = ++num;
                        j--;
                        if (i == 0 && j == 0) {
                            break outofloop;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < ulam.length; i++) {
            for (int j = 0; j < ulam[i].length; j++) {
                int col = 2;
                for (int del = 2; del <= ulam[i][j]; del++) {
                    if (ulam[i][j] % del == 0 && del != ulam[i][j]) {
                        col++;
                    }
                }
                if (col < 3 && ulam[i][j] != 0) {
                    System.out.print(" * ");
                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }
}


