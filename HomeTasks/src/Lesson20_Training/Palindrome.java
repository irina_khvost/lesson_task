package Lesson20_Training;

import java.util.Scanner;

/**
 * Числа, одинаково читающиеся слева направо и справа налево, называются палиндромами. Например, 1223221.
 * Напишите программу нахождения всех палиндромов на данном отрезке.
 */

public class Palindrome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите конечное число интервала для поиска палиндромов: ");
        String s = sc.nextLine();
        if (Amount_Sum_Revers_Number.tryParseInt(s)) {
            int num = Integer.parseInt(s);
            if (num > 11) {
                for (int i = 11; i <= num; i++) {
                    if (searchPalindrom(String.valueOf(i)))
                        System.out.println(i);
                }
            } else System.out.println("Палиндромов нет.");
        } else {
            System.out.println("Error! Вы ввели не число!");
        }
    }

    //Проверка палиндром ли это
    static boolean searchPalindrom(String s) {
        boolean flag = true;
        for (int i = 0; i < s.length() / 2; ) { //i++
            if (s.charAt(0) == s.charAt(s.length() - 1)) {
                s = s.substring(1, s.length() - 1);
            } else {
                flag = false;
                break;
            }
        }
        return flag;
    }
}
