package Lesson20_Training;
/*
Л. Кэрролл в своем дневнике писал, что он тщетно трудился, пытаясь найти хотя бы три
прямоугольных треугольника равной площади, у которых длины сторон были бы выражены
натуральными числами. Составьте программу для решения этой задачи, если известно, что такие
треугольники существуют. Напишите программу, которая находит все прямоугольные треугольники
(длины стороны выражаются натуральными числами), площадь которых не превышает данного числа S.
 */

import java.util.*;

public class Task20_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число максимум для поиска: ");
        int n = sc.nextInt();
        ArrayList<Karl> list = new ArrayList<>();
        Karl karl = new Karl();
        karl.karlNumber(n, list);
        karl.print(n, list);


    }
}

class Karl {
    int a;
    int b;
    int c;
    int sqr;

    public Karl() {
    }

    public Karl(int a, int b, int c, int sqr) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.sqr = sqr;
    }

    public int getSqr() {
        return sqr;
    }

    void karlNumber(int n, ArrayList<Karl> list) {
        for (int c = 1; c <= n; c++) {
            for (int b = 1; b <= c; b++) {
                for (int a = 1; a <= b; a++) {
                    if ((int) Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2)) {
                        int k = (a * b) / 2;
                        list.add(new Karl(a, b, c, k));
                    }
                }
            }
        }
    }

    void print(int n, ArrayList<Karl> list) {
        list.sort(Comparator.comparing(Karl::getSqr));
        ArrayList<Karl> out = new ArrayList<>();
        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i).equals((Object)list.get(i + 1)) && list.get(i).sqr <= n) {
                out.add(new Karl(list.get(i).a, list.get(i).b, list.get(i).c, list.get(i).sqr));
                out.add(new Karl(list.get(i + 1).a, list.get(i + 1).b, list.get(i + 1).c, list.get(i + 1).sqr));
            }
        }
        for (int i = 0; i < out.size() - 1; i++) {
            if (out.get(i).equals(out.get(i + 1))){
                out.remove(i);
                i--;
            }
        }

        for (Karl karl : out) {
            System.out.println(karl);
        }
    }

   @Override
    public String toString() {
        return a + " "+  b + " "+  c + " "+ sqr;
    }

    @Override
    public boolean equals(Object obj) {
        return this.sqr == ((Karl)obj).sqr;
    }

    public boolean equals(Karl obj) {
        return this.a == obj.a && this.b == obj.b;
    }


}


