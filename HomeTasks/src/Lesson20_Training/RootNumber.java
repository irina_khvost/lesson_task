package Lesson20_Training;

import java.util.Scanner;

/**
 * Если сложить все цифры какого-либо натурального числа, затем — все цифры найденной суммы и так далее,
 * то в результате получим однозначное число (цифру), которое называется цифровым корнем данного числа.
 * Например, цифровой корень числа 561 равен 3 (5 + 6+1 — 12, 1+2 = 3).
 * Написать программу для нахождения числового корня данного натурального числа.
 */

public class RootNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число: ");
        String s = sc.nextLine();
        if (Amount_Sum_Revers_Number.tryParseInt(s)) {
            System.out.println("Корень числа равно: " + root(s));
        } else {
            System.out.println("Error! Вы ввели не число!");
        }
    }

    static int root(String s) {
        int r = 0;
        if (s.length() < 2)
            r = Integer.parseInt(s);
        else {
            for (int i = 0; i < s.length(); i++) {
                r = r + Character.getNumericValue(s.charAt(i));
            }
            return root(String.valueOf(r));
        }
        return r;
    }
}
