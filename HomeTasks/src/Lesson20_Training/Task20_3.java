package Lesson20_Training;
/*
Напишите программу, которая определяет, сколько можно купить быков, коров и телят,
платя за быка 10 рублей, за корову - 5 рублей, а за теленка - 50 копеек, если на 100 рублей надо
купить 100 голов скота?
 */

public class Task20_3 {
    public static void main(String[] args) {
        int bull;
        int cow;
        int calf;

        double allMoney = 100;
        int bullMoney = 10;
        int cowMoney = 5;
        double calfMoney = 0.5;

        for (bull = 0; bull <= allMoney / bull; bull++) {
            for (cow = 0; cow <= (allMoney - bullMoney * bull) / cow; cow++) {
                for (calf = 0; calf <= (allMoney - bullMoney * bull - cowMoney * cow) / calfMoney; calf++) {
                    if (allMoney - bullMoney * bull - cowMoney * cow - calfMoney * calf == 0 && bull + cow + calf == 100) {
                        System.out.println("All: " + bull + " bull, " + cow + " cow, " + calf + " calf");
                    }
                }
            }
        }
    }
}
