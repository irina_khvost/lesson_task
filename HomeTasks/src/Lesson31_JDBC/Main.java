package Lesson31_JDBC;

import java.sql.*;

public class Main {
    final static String DB_DRIVER = "com.mysql.jdbc.Driver";
    final static String DB_CONNECTION = "jdbc:mysql://localhost:3306/test?" +
            "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    final static String DB_USER = "root";
    final static String DB_PASSWORD = "";

    public static void main(String[] args) {
       // addVal("Misha", 2356);
        getAllData();

    }

    private static Connection getDBConnection() {
        Connection dbConection = null;
        try {
            Class.forName(DB_DRIVER);
            dbConection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConection;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return dbConection;
    }

    private static void addVal(String name, int count) {
        String sqlInsert = "INSERT INTO `table1` (`name`, `count`)\n" +
                "VALUES ('"+ name +"', '" + count +"');";
        Connection dbConnection = null;
        Statement statement = null;
        dbConnection = getDBConnection();
        try {
            statement = dbConnection.createStatement();
            statement.execute(sqlInsert);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("INSERT LINE");
    }

    private static void getAllData() {
        String sqlSelect = "SELECT * \n" +
                "FROM table1 \n" +
                "ORDER BY count";
        try {
            Connection dbConnection = null;
            Statement statement = null;
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();
            ResultSet rs = statement.executeQuery(sqlSelect);

            while (rs.next()) {
                String name = rs.getString("name");
                String count = rs.getString("count");

                System.out.print("name: " + name);
                System.out.println(" count: " + count);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("SELECT ALL DATA");
    }

}

