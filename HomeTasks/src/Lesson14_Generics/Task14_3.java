package Lesson14_Generics;

/*
Создать функцию, в которую передаются два значения, каждое из которых
может являться либо целочисленным, либо вещественным, после чего
определяется тип, и функция возвращает их сумму либо в виде целочисленного,
либо в виде вещественного значения. */

public class Task14_3 {
    public static void main(String[] args) {
        System.out.println(ZnachSum.sum(5, 7));
        System.out.println(ZnachSum.sum(5.5, 7.7));
    }
}

class ZnachSum {
    static <T> T sum(T x, T y) {
        if (x instanceof Integer) {
            Integer res = ((Integer) x).intValue() + ((Integer) y).intValue();
            return (T) res;
        }
        if (x instanceof Double) {
            Double res = ((Double) x).doubleValue() + ((Double) y).doubleValue();
            return (T) res;
        }
        return x;
    }
}
