package Lesson14_Generics;
/*
Создать функцию, в которую передается логическое, либо целочисленное
значение, после чего распознается тип переданных данных. Если было передано
логическое значение false, либо целочисленное значение 0, функция возвращает
строку “Верно”, во всех остальных случаях возвращается строка “Неверно”.
 */

public class Task14_2 {
    public static void main(String[] args) {
        System.out.println(Znach.<Integer>none(5));
        System.out.println(Znach.<Integer>none(0));
        System.out.println(Znach.<Boolean>none(false));
        System.out.println(Znach.<Boolean>none(true));
    }
}

class Znach {
    static <T> String none(T vol) {
        String rez = "Неверно";
        if (vol instanceof Boolean) {
            if (!(Boolean) vol)
                return rez = "Верно";
        }

        if (vol instanceof Integer) {
            if ((Integer) vol == 0) {
                return rez = "Верно";
            }
        }
        return rez;
    }
}

