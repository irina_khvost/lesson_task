package Lesson14_Generics;

/*
Создать класс “Elf” с полями для показателей здоровья и урона и функцией
“Strike” типа void, от которого наследовать 3 класса “ElfWarrior”, “ElfArcher”, “ElfMage”,
в каждом из которых переопределить функцию “Strike”, также для каждого из
классов переопределить функцию toString, через которую будет выводиться
информация о классе. Написать класс-обобщение, который принимает на вход только класс “Elf” и его потомков.
 */
public class Task14_6 {
    public static void main(String[] args) {
        ElfArcher elfArcher = new ElfArcher(100, 50);
        OverElf.fancion(elfArcher);
        ElfMage elfMage = new ElfMage(10, 500);
        OverElf.fancion(elfMage);
        ElfWarrior elfWarrior = new ElfWarrior(50, 20);
        OverElf.fancion(elfWarrior);
    }
}

class Elf {
    int hp;
    int damage;

    Elf(int hp, int damage) {
        this.hp = hp;
        this.damage = damage;
    }

    public void strike() {
    }
}

class ElfArcher extends Elf {
    public ElfArcher(int hp, int damage) {
        super(hp, damage);
    }

    @Override
    public void strike() {
        System.out.println("ElfArcher! hp = " + hp + " damage = " + damage);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}

class ElfMage extends Elf {
    public ElfMage(int hp, int damage) {
        super(hp, damage);
    }

    @Override
    public void strike() {
        System.out.println("ElfMage! hp = " + hp + " damage = " + damage);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}

class ElfWarrior extends Elf {
    public ElfWarrior(int hp, int damage) {
        super(hp, damage);
    }

    @Override
    public void strike() {
        System.out.println("ElfWarrior! hp = " + hp + " damage = " + damage);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}

class OverElf {
    public static <T extends Elf> void fancion(T t) {
        t.strike();
        System.out.println(t.toString());
    }
}

