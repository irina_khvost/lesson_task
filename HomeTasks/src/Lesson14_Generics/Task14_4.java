package Lesson14_Generics;

/*
Создать функцию, в которую передается либо символьное, либо строковое
значение, после чего вычисляется и возвращается код символа, либо сумма кодов
символов.
*/
public class Task14_4 {
    public static void main(String[] args) {
        System.out.println(ZnachChToStr.none('r'));
        System.out.println(ZnachChToStr.none("rrr"));
    }
}

class ZnachChToStr {
    static <T> int none(T vol) {
        int ch = 0;
        if (vol instanceof Character)
            ch = (int) ((Character) vol).charValue();

        if (vol instanceof String) {
            char[] chArr = ((String) vol).toCharArray();
            for (int i = 0; i < chArr.length; i++) {
                ch += (int) ((Character) chArr[i]).charValue();
            }
        }
        return ch;
    }
}
