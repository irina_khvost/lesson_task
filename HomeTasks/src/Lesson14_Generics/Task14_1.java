package Lesson14_Generics;

/*
Создать функцию, в которую передается либо целочисленное, либо
вещественное, либо строковое значение, после чего на экран выводится тип
переданных в функцию данных и само значение
if (t instanceof Boolean[]){
Boolean[] tmp = ((Boolean[]) t)
} */

public class Task14_1 {
    public static void main(String[] args) {
        Abstr.<Integer>none(67);
        Abstr.<Boolean>none(false);
        Abstr.<Double>none(67.0);
        Abstr.<Float>none(67f);
//        Abstr.none(new Hero());
    }
}

/*class Hero{
    int hp = 60;
    @Override
    public String toString() {
        return "It hero of my brain! " + hp;
    }
}*/
class Abstr {
    static <T> void none(T vol) {
        System.out.println(vol.getClass().getSimpleName() + "  " + vol.toString());
    }
}

