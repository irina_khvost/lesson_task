package Lesson14_Generics;

import java.util.Arrays;

/*
Создать функцию, в которую передается массив логических,
целочисленных, либо вещественных значений, после чего распознается тип
данных в массиве и уже в пределах функции создается массив с полученным
типом, в который записываются переданные в функцию значения.
 */
public class Task14_5 {
    public static void main(String[] args) {
        ZnachArr.<Integer>none(new Integer[]{10, 20, 30, 40});
        ZnachArr.<Boolean>none(new Boolean[]{false, false, true, true});
        ZnachArr.<Double>none(new Double[]{10.1, 20.5, 30.6, 40.4});
    }
}

class ZnachArr {
    static <T> void none(T[] vol) {
        if (vol instanceof Boolean[]) {
            Boolean[] tmp = (Boolean[]) vol;
            System.out.println(Arrays.toString(tmp));
        }
        if (vol instanceof Integer[]) {
            Integer[] tmp = (Integer[]) vol;
            System.out.println(Arrays.toString(tmp));
        }
        if (vol instanceof Double[]) {
            Double[] tmp = (Double[]) vol;
            System.out.println(Arrays.toString(tmp));
        }
    }
}
