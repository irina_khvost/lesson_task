package Lesson30_ThreadsAll;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Random;

public class Resource {
    private Type[] data;
    int jobs; //сколько было произведено действий
    int countOfRead; //счётчик, по которому можно брать данные

    Resource() {
        createData();
        jobs = 0;
        countOfRead = 0;
    }

    public Resource(Type[] data) {
        this.data = data;
        jobs = 0;
        countOfRead = 0;
    }

    //Функция заполнения data
    private void createData() {
        Random rnd = new Random();
        String s = "";
        String fileName = "C:\\Users\\Sacinandan Kisor das\\IdeaProjects\\HomeTasks\\src\\tmp\\IOFile";
        data = new Type[50];
        if ((new File(fileName)).exists()) {
            readFile(fileName);
        } else {
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < 6; j++) {
                    s += String.valueOf((char) (rnd.nextInt(25) + 65));
                }
                data[i] = new Type(s);
                s = "";
            }
        }
    }

    //Функция возваращает указатель на одну из ячеек и увеличивает счётчик по которому можно брат на еденицу
    // ячейку данных типа Type
    synchronized Type getStartValue() {
        return data[countOfRead++];
    }

    //Функция, которая показывает, что было произведене действие с ячейкой данных
    synchronized void jobDone() {
        jobs++;
    }

    //Функция, которая возращяет копию данных массив типа Type с копией данных
    synchronized Type[] getAllData() {
        Type[] tmpData = new Type[data.length];
        for (int i = 0; i < data.length; i++) {
            tmpData[i] = new Type(data[i].str);
        }
        return tmpData;
    }

    void printData() {
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i].str + " ");
            if ((i+1) % 10 == 0) {
                System.out.println();
            }
        }
        System.out.println();
    }

    //Функция проверки не достигли ли конца массива true если индекс того,
    // какую ячейку брать меньше длины массива, false если больше либо равен
    boolean canDo() {
        return countOfRead < data.length;
    }

    Type[] readFile(String fileName) {
        try (FileInputStream fis = new FileInputStream(fileName);
             BufferedInputStream bis = new BufferedInputStream(fis)) {
            int c;
            int i = 0;
            String s = "";
            while ((c = bis.read()) != -1) {
                if (c != 13) {
                    s = s + (char) c;
                } else {
                    data[i] = new Type(s);
                    i++;
                    s = "";
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
}
