package Lesson30_ThreadsAll;

public class Calculate extends Thread {
    private Resource resource;

    public Calculate(Resource resource) {
        this.resource = resource;
    }

    @Override
    public void run() {
        while (resource.canDo()) {//поток будет работать до тех пор, пока ресурс сообщает, что работа возможна
            if (resource.canDo()) {//дополнительная проверка, чтобы не выйти за границы массива
                System.out.println(Thread.currentThread().getName() + /*" - " + increment() + */" - " + decrement());
                resource.jobDone();
            }
            try {
                Thread.sleep((long) (Math.random() * 500));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
        System.out.println(Thread.currentThread().getName() + " was end");
    }

    String increment(){
        Type tmp;
        String tmpSTR;
        tmp = resource.getStartValue();
        tmpSTR = tmp.str;
        tmp.str = "";
        for (int i = 0; i < tmpSTR.length(); i++) {
            tmp.str += (char) ((int) tmpSTR.charAt(i) + 1);
        }
        return tmp.str;
    }

    String decrement(){
        Type tmp;
        String tmpSTR;
        tmp = resource.getStartValue();
        tmpSTR = tmp.str;
        tmp.str = "";
        for (int i = 0; i < tmpSTR.length(); i++) {
            tmp.str += (char) ((int) tmpSTR.charAt(i) - 1);
        }
        return tmp.str;
    }


}
