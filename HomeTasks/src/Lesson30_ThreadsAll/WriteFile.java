package Lesson30_ThreadsAll;

import java.io.*;

public class WriteFile extends Thread {
    private Resource resource;
    private int countOfBackup;

    public WriteFile(Resource resource) {
        this.resource = resource;
        countOfBackup = 0;
    }

    @Override
    public void run() {
        //переменная хранящая шаблон пути файла
        String wayToFile = "C:\\Users\\Sacinandan Kisor das\\IdeaProjects\\HomeTasks\\src\\tmp\\file";
        int counWait = 0;
        while (counWait <= 5) {
            System.out.println(resource.jobs);
            if (resource.jobs >= 10) { //если было произведено 10 "работ" с данными то делаем бэкап
                wayToFile += countOfBackup++; //меняем название файла, чтобы каждый раз создавался новый
                wayToFile += ".irina";
                writeToFile(wayToFile); //пишем файл
                resource.jobs -= 10;//и уменьшаем счетчик работ на 10 тем самым показывая, что 10 изменений мы записали
                wayToFile = "C:\\Users\\Sacinandan Kisor das\\IdeaProjects\\HomeTasks\\src\\tmp\\file";//обнуляем шаблон записи
                counWait = 0;
            }
            try {
                Thread.sleep(100);
                if (resource.jobs == 0)
                    counWait++;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        delFile();
    }

    void writeToFile(String fileName) {
        try (FileWriter fileWriter = new FileWriter(fileName, false)) {
            System.out.println("Start write to file " + fileName);
            String res = "";//строка которая будет записана в файл. Далее её формируем
            Type[] data = resource.getAllData();//получаем копию данных для записи в файл
            for (int i = 0; i < data.length; i++) {//формируем строку для записи
                res += data[i].str;
                res += "\r";
            }
            fileWriter.write(res);//пишем в файл
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("End write to file " + fileName);
    }

    void delFile() {
        String fileName = "C:\\Users\\Sacinandan Kisor das\\IdeaProjects\\HomeTasks\\src\\tmp\\";
        final File folder = new File(fileName);
        final File[] files = folder.listFiles(new FilenameFilter() { //регулярное выражение на поиск файла по маске и уделение его
            @Override
            public boolean accept(final File dir,
                                  final String name) {
                return name.matches(".*\\.irina");
            }
        });
        for (final File file : files) {
            if (!file.delete()) {
                System.err.println("Can't remove " + file.getAbsolutePath());
            }
        }
    }
}
