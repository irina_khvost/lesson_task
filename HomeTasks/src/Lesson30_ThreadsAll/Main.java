package Lesson30_ThreadsAll;

/**
 * Создать класс общеступный ресурс, который будет содержать в себе массив с
 * данными содержащий в себе строку и уникальную позицию. Строка генерируется и состоит из
 * случайных символов. Создать два класса для работы с потоками. Один из них записывает
 * данные в файл каждые 10 изменений данных в общем массиве и работает бесконечно. Второй
 * берет одну строку из массива и меняет все буквы в строке следующим образом. Например
 * строка ABBCCA, генерируется случайное число(например два) и каждый символ в строке
 * меняется на следующий с учётом сдвига в алфавите. Получаем строку BCCDDB.
 * Программа должна считать текст из файла. После выполнения программы файлы “бэкапов” удаляются.
 */

public class Main {
    public static void main(String[] args) {
        Resource resource = new Resource();
        resource.printData();
        Calculate first = new Calculate(resource);
        Calculate second = new Calculate(resource);
        Calculate third = new Calculate(resource);
        WriteFile four = new WriteFile(resource);
        first.start();
        second.start();
        third.start();
        four.start();
        try {
            first.join();
            second.join();
            third.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        resource.printData();
    }
}



