package Lesson19_ArrayList;
/*
Создать абстрактный класс Фигура с абстрактным методом draw. Отнаследовать от него 5 типов фигур. Для каждого реализовать
отрисовку этой фигуры в консоли. В main создать arraylist фигур. В цикле создать генератор фигур, который
будет случайным образом выдавать фигуру и она будет записываться в имеющийся arraylist. В конце вывести фигуры на экран.
 */

import java.util.ArrayList;

public class Task19_1 {
    public static void main(String[] args) {
        ArrayList<Figura> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int a = (int) (Math.random() * 5);
            if (a == 0) list.add(new Square());
            if (a == 1) list.add(new Rectangle());
            if (a == 2) list.add(new Triangle());
            if (a == 3) list.add(new Rhombus());
            if (a == 4) list.add(new Trapezium());
        }

        for (int i = 0; i < list.size(); i++) {
            list.get(i).draw();
            System.out.println("----------------------------");
        }
    }
}

abstract class Figura {
    abstract void draw();
}

class Square extends Figura {
    @Override
    void draw() {
        System.out.println("* * * *");
        System.out.println("*     *");
        System.out.println("*     *");
        System.out.println("* * * *");
    }
}

class Rectangle extends Figura {
    @Override
    void draw() {
        System.out.println("* * * * * *");
        System.out.println("*         *");
        System.out.println("* * * * * *");
    }
}

class Triangle extends Figura {
    @Override
    void draw() {
        System.out.println("* ");
        System.out.println("* * ");
        System.out.println("*   * ");
        System.out.println("*     * ");
        System.out.println("* * * * * ");
    }
}

class Rhombus extends Figura {
    @Override
    void draw() {
        System.out.println("      * ");
        System.out.println("    *   * ");
        System.out.println("  *       * ");
        System.out.println("    *   * ");
        System.out.println("      *");
    }
}

class Trapezium extends Figura {
    @Override
    void draw() {
        System.out.println("* * * * * * *");
        System.out.println("  *       *  ");
        System.out.println("    * * *    ");
    }
}





