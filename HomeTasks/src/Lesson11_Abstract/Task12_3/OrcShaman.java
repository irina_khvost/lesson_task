package Lesson11_Abstract.Task12_3;

public class OrcShaman extends Orc {
    public OrcShaman(int damage, int armor, int hp) {
        super(damage, armor, hp, "magical");

    }

    @Override
    public void Attack() {
        System.out.println("Damage: " + damage + ", Type of attack: " + typeAttack);
    }
}
