package Lesson11_Abstract.Task12_3;

public class Main {
    public static void main(String[] args) {
        OrcGrunt orcGrunt = new OrcGrunt(100,20,300);
        orcGrunt.Attack();
        System.out.println(orcGrunt.hashCode());
        System.out.println(orcGrunt.toString());
        OrcPeon orcPeon = new OrcPeon(20,30,100);
        orcPeon.Attack();
        OrcShaman orcShaman = new OrcShaman(300, 20,60);
        orcShaman.Attack();
    }
}
