package Lesson11_Abstract.Task12_3;

public class OrcPeon extends Orc {

    public OrcPeon(int damage, int armor, int hp) {
        super(damage, armor, hp, "crushing");
    }

    @Override
    public void Attack() {
        System.out.println("Damage: " + damage + ", Type of attack: " + typeAttack);

    }
}
