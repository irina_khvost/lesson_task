package Lesson11_Abstract.Task12_3;

public class OrcGrunt extends Orc {
    public OrcGrunt(int damage, int armor, int hp) {
        super(damage, armor, hp, "hacking");
    }

    @Override
    public void Attack() {
        System.out.println("Damage: " + damage + ", Type of attack: " + typeAttack);
    }

    @Override
    public int hashCode() {
        int hashCode= 999;
        return hashCode;
    }

    @Override
    public String toString() {
        return "The function was override to toString";
    }
}
