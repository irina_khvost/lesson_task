package Lesson11_Abstract.Task12_3;
/*
Создать абстрактный класс “Orc” с переменными для показателей урона, брони, здоровья и типа атаки,
содержащий также абстрактный метод Attack. Наследовать от него 3 обычных класса “OrcPeon”,
“OrcGrunt” и “OrcShaman”, для каждого из которых установить тип атаки ”crushing”, “hacking” и “magical”(как
переменная класса типа String), также определить для каждого из классов переменные из абстрактного класса и
функцию Attack, чтобы она выводила на экран показатель атаки и ее тип.
 */
public abstract class Orc {
    int damage;
    int armor;
    int hp;
    String typeAttack;

    public Orc(int damage, int armor, int hp, String typeAttack) {
        this.damage = damage;
        this.armor = armor;
        this.hp = hp;
        this.typeAttack = typeAttack;
    }

    public abstract void Attack();

}
