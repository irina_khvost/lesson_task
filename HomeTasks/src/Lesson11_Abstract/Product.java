package Lesson11_Abstract;

public class Product {
    public static void main(String[] args) {
        Chek chek = new Chek();
        chek.setName("Сахар");
        chek.setPrice(2.5);
        chek.setWeight(1);
        chek.setCol(2);
        chek.print();
    }
}

class Product1 {
    private String name;
    private double price;
    private double weight;

    public Product1() {

    }

    public Product1(String name, double price, double weight) {
        this.name = name;
        this.price = price;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}

class Buy extends Product1 {
    private int col;
    private double total;
    private double fullWeight;

    public Buy() {
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public double getTotal() {
        return total = super.getPrice() * col * super.getWeight();
    }

   /* public void setTotal(double total) {
        this.total = total;
    }*/

    public double getFullWeight() {
        return fullWeight = col * super.getWeight();
    }

  /*  public void setFullWeight(double fullWeight) {
        this.fullWeight = fullWeight;
    }*/

    public Buy(String name, double price, double weight, int col, double total, double fullWeight) {
        super(name, price, weight);
        this.col = col;
        this.total = total;
        this.fullWeight = fullWeight;
    }
}

class Chek extends Buy {
    void print() {
        System.out.println("Имя товара " + getName() + " цена " + getPrice() +
                        " вес продукта " + getWeight() + " кол-во товара " + getCol() +
                        "\n итого " + getTotal() + " вес итого " + getFullWeight()
                );

    }
}

/*
Разработать три класса, которые следует связать между собой, используя наследование:
класс Product, который имеет три элемент-данных — имя,цена и вес товара (базовый класс для всех классов);
класс Buy, содержащий данные о количестве покупаемого товара в штуках, о цене за весь купленный товар и о весе
товара (производный класс для класса Product и базовый класс для класса Check);
класс Check, не содержащий никаких элемент-данных.
Данный класс должен выводить на экран информацию о товаре и о покупке ( производный класс для класса Buy);
Для взаимодействия с данными классов разработать set- и get—методы. Все элемент-данные классов объявлять как
private.
 */