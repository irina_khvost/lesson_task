package Lesson11_Abstract;

public class Calculator {
    public static void main(String[] args) {
        MyCalc mycalc = new MyCalc();
        mycalc.setX(25);
        System.out.println("Сумма: " + mycalc.sum(10));
        System.out.println("Разность: " + mycalc.difference(10));
        System.out.println("Умножение: " + mycalc.multiple(10));
        System.out.println("Деление: " + mycalc.division(10));
        System.out.println("Корень: " + mycalc.sqrt());
        System.out.println("Синус: " + mycalc.sin());
        System.out.println(mycalc.getX());
        System.out.println("Сумма дочернего: " + mycalc.sum(20));


    }
}

class Calc {
    double x;

    public Calc() {
        x = 0;
    }

    public Calc(double x) {
        this.x = x;
    }

    public double sum(double y) {
        return x += y;
    }

    public double difference(double y) {
        return x -= y;
    }

    public double multiple(double y) {
        return x *= y;
    }

    public double division(double y) {
        return x /= y;
    }

    public double sqrt() {
        return x = Math.sqrt(x);
    }

    public double sin() {
        return x =  Math.sin(x);
    }

}

class MyCalc extends Calc {

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double sum(double x) {
        return super.sum(x);
    }

}

/*
Написать класс калькулятора, хранящего вещественное число x и понимающего следующие команды:
прибавить к этому числу значение параметра, вычесть из него, домножить его и разделить, а также извлечь из этого числа
квадратный корень и взять тригонометрическую функцию.
Написать еще один класс, кроме перечисленного имеющий одну ячейку памяти и понимающий команды записать в память,
извлечь из памяти, добавить x к содержимому памяти.
 */
