package Lesson11_Abstract.Task12_1;

/*
Создать абстрактный класс “Dog” с переменными для клички, пола, веса собак,
содержащий также абстрактные методы Voice и Action. Наследовать от него три
обычных класса “DomesticDog”, “ServiceDog” и “HomelessDog”, для каждого из которых
определить строковую переменную address, organization, area соответственно.
 */
public abstract class Dog {
    String name;
    String sex;
    double weigth;

    public Dog(String name, String sex, double weigth) {
        this.name = name;
        this.sex = sex;
        this.weigth = weigth;
    }

    public abstract void voice();
    public abstract void action();
}

