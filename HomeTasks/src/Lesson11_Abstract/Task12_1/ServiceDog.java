package Lesson11_Abstract.Task12_1;

public class ServiceDog extends Dog {
    String organization;

    public ServiceDog(String name, String sex, double weigth, String organization) {
        super(name, sex, weigth);
        this.organization = organization;
    }

    @Override
    public void voice() {
        System.out.println("Gay - gay");
    }

    @Override
    public void action() {
        System.out.println("Always pills");
    }

    void print() {
        System.out.println("Service dog: Name: " + this.name + " sex: " + this.sex +
                " weigth: " + this.weigth + " address: " + this.organization);
    }
}
