package Lesson11_Abstract.Task12_1;

public class DomesticDog extends Dog {
    String address;

    public DomesticDog(String name, String sex, double weigth, String address) {
        super(name, sex, weigth);
        this.address = address;
    }

    @Override
    public void voice() {
        System.out.println("Tiay - tiay");
    }

    @Override
    public void action() {
        System.out.println("Always sleep");
    }

    void print() {
        System.out.println("Domestic Dog: Name: " + this.name + " sex: " + this.sex +
                " weigth: " + this.weigth + " address: " + this.address);
    }

}




