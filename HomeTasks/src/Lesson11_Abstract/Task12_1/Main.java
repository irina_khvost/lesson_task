package Lesson11_Abstract.Task12_1;

public class Main {
    public static void main(String[] args) {
        DomesticDog domesticDog = new DomesticDog("Lyiz", "cable", 10.5, "Pl. Lenina 45");
        domesticDog.print();
        domesticDog.voice();
        domesticDog.action();
        ServiceDog serviceDog = new ServiceDog("Myhtar", "cable", 8, "police");
        serviceDog.print();
        serviceDog.voice();
        serviceDog.action();
        HomelessDog homelessDog = new HomelessDog("Shavka", "girl", 3, "trash");
        homelessDog.print();
        homelessDog.voice();
        homelessDog.action();

    }
}
