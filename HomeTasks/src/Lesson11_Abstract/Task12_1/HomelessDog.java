package Lesson11_Abstract.Task12_1;

public class HomelessDog extends Dog {
    String area;

    public HomelessDog(String name, String sex, double weigth, String area) {
        super(name, sex, weigth);
        this.area = area;
    }

    @Override
    public void voice() {
        System.out.println("Ay...");
    }

    @Override
    public void action() {
        System.out.println("Always food search");
    }


    void print() {
        System.out.println("Homeless dog: Name: " + this.name + " sex: " + this.sex +
                " weigth: " + this.weigth + " address: " + this.area);
    }
}
