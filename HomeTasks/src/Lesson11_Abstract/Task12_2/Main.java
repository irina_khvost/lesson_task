package Lesson11_Abstract.Task12_2;

public class Main {
    public static void main(String[] args) {
        Rat rat = new Rat("Крыса", 1, "белый", 0.3);
        rat.run();
        Chinchilla chinchilla = new Chinchilla("Шиншила", 3.5, "серый", 0.8);
        chinchilla.run();
        Hamster hamster = new Hamster("Хомяк", 0.1, "рыжий", 0.2);
        hamster.run();

    }
}
