package Lesson11_Abstract.Task12_2;

public class Rat extends Rodent {

    public Rat(String name, double weight, String color, double speed) {
        super(name, weight, color, speed);

    }

    @Override
    public void run() {
        System.out.println("Name class: " +getClass().getSimpleName() + " speed " + speed);

    }

    @Override
    public void jump() {

    }

    @Override
    public void eat() {

    }
}
