package Lesson11_Abstract.Task12_2;
/*
Создать абстрактный класс “Rodent” с переменными для пола, веса, цвета шкуры
и скорости бега, содержащий также абстрактные методы Run, Jump и Eat. Наследовать
от него 3 обычных класса “Hamster”, “Chinchilla” и “Rat”, для каждого из которых
переопределить метод Run, чтобы он выводил название класса и скорость бега грызуна
на экран.
 */

public abstract class Rodent {
    String name;
    double weight;
    String color;
    double speed;

    public Rodent(String name, double weight, String color, double speed) {
        this.name = name;
        this.weight = weight;
        this.color = color;
        this.speed = speed;
    }

    public abstract void run();

    public abstract void jump();

    public abstract void eat();
}
