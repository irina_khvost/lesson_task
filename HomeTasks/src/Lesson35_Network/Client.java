package Lesson35_Network;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

import static Lesson35_Network.Server.recvString;
import static Lesson35_Network.Server.sendString;


public class Client {
    public static void main(String[] args) {
        Socket s = null;
        try {
            s = new Socket("192.168.10.215", 9999);
        } catch (Exception ex) {
            System.out.println(ex.toString());
            System.exit(0);
        }

        int nPort = s.getLocalPort();
        System.out.println("Local Port: " + nPort);

        InputStream is;
        OutputStream os;

        try {
            is = s.getInputStream();
            os = s.getOutputStream();
            String szStr;
            Scanner scanner = new Scanner(System.in);
            while (true) {
                szStr = scanner.next();
                sendString(os, szStr);
                os.flush();
                if (szStr.equals("quit"))
                    break;
                szStr = recvString(is);
                System.out.println(szStr);
            }
            is.close();
            os.close();
            s.close();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
    }
}
