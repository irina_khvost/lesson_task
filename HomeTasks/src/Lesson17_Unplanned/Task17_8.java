package Lesson17_Unplanned;
/*
Описать класс, представляющий треугольник. Предусмотреть методы для создания
объектов, вычисления площади, периметра и точки пересечения медиан.
 */

public class Task17_8 {
    public static void main(String[] args) {
        Treangal treangal = new Treangal(0, 0, 0, 3, 4, 3);
        System.out.println("AB = " +treangal.ab + " BC = " + treangal.bc + " AC = " + treangal.ac);
        System.out.printf("Периметр: %.2f \n", treangal.perimetr());
        System.out.printf("Площадь: %.2f \n", treangal.area());
        System.out.println("Точки пересечения медиан: " + treangal.centr());
    }
}

class Treangal {
    private int x1, y1;
    private int x2, y2;
    private int x3, y3;
    double ab, bc, ac;

    public Treangal(int x1, int y1, int x2, int y2, int x3, int y3) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
        ab = Math.sqrt(Math.pow((y2 - y1), 2) + Math.pow((x2 - x1), 2));
        bc = Math.sqrt(Math.pow((y3 - y2), 2) + Math.pow((x3 - x2), 2));
        ac = Math.sqrt(Math.pow((y3 - y1), 2) + Math.pow((x3 - x1), 2));
    }

    double perimetr() {
        return ab + bc + ac;
    }

    double area() {
        return Math.abs((x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1)) / 2;
    }

    String centr() {
        return "x0 = " + ((x1 + x2 + x3) / 3) + "; y0 = " + ((y1 + y2 + y3) / 3);
    }
}
