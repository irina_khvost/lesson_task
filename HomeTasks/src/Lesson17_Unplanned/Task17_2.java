package Lesson17_Unplanned;
/*
Дан массив. Перемешать его элементы случайным образом так, чтобы каждый элемент
оказался на новом месте
 */

public class Task17_2 {
    public static void main(String[] args) {
        int[] array = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        for (int e : array) {
            System.out.print(e + " ");
        }
        System.out.println();
        for (int i = 0; i < array.length - 1; i += 2) {
            int tmp = array[i];
            array[i] = array[i + 1];
            array[i + 1] = tmp;
        }

        if (array.length % 2 != 0) {
            int tmp = array[0];
            array[0] = array[array.length - 1];
            array[array.length - 1] = tmp;
        }

        for (int e : array) {
            System.out.print(e + " ");
        }
    }
}
