package Lesson17_Unplanned;
/*
Сформировать квадратную матрицу n x n, на диагонали которой находятся случайные
числа из диапазона [1; 9], а остальные числа равны 1.
 */

import java.util.Arrays;

public class Task17_6 {
    public static void main(String[] args) {
        int[][] array = new int[5][5];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i == j) array[i][j] = (int) (Math.random() * 9 + 1);
                else array[i][j] = 1;
            }
            System.out.println(Arrays.toString(array[i]));
        }
    }
}
