package Lesson17_Unplanned;
/*
Создать квадратную матрицу, на диагонали которой находятся тройки, выше диагонали
находятся двойки, остальные элементы равна единице.
*/

import java.util.Arrays;

public class Task17_5 {
    public static void main(String[] args) {
        int[][] array = new int[3][3];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++) {
                if (i == j) array[i][j] = 3;
                if (i < j) array[i][j] = 2;
                if (i > j) array[i][j] = 1;
            }
            System.out.println(Arrays.toString(array[i]));
        }
    }
}
