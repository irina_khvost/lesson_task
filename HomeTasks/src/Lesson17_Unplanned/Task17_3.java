package Lesson17_Unplanned;
/*
Дано предложение. Заменить группы пробелов одиночными, крайние пробелы удалить.
Все слова перевести в нижний регистр, первые буквы сделать заглавными.
 */

import java.util.ArrayList;
import java.util.Scanner;

public class Task17_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter please text: ");
        String str = sc.nextLine().toLowerCase();
        ArrayList<Character> arrayList = new ArrayList<>();

        str = str.trim();
        for (int i = 0; i < str.length(); ) {
            if (str.charAt(i) == ' ' && str.charAt(i + 1) != ' ') { //comparison character with space
                arrayList.add(str.charAt(i));
                arrayList.add(Character.toUpperCase(str.charAt(i + 1)));
                i += 2;
            }
            if (str.charAt(i) != ' ') {  //output character
                arrayList.add(str.charAt(i));
            }
            i++;
        }
        arrayList.set(0,Character.toUpperCase(arrayList.get(0))); //do the first character UpperCase

        for (Character character : arrayList) {
            System.out.print(character);
        }
    }
}
