package Lesson17_Unplanned;

import java.util.Scanner;

/*
Составить описание класса для представления времени. Предусмотреть возможности
установки времени и изменения его отдельных полей (час, минута, секунда) с проверкой
допустимости вводимых значений. В случае недопустимых значений полей выбрасываются
исключения. Создать методы изменения времени на заданное количество часов, минут и секунд
 */
public class Task17_9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Clock clock = new Clock(0, 0, 0);
        System.out.print("Введите часы: ");
        clock.setHour(sc.nextInt());
        System.out.print("Введите минуты: ");
        clock.setMinutes(sc.nextInt());
        System.out.print("Введите секунды: ");
        clock.setSeconds(sc.nextInt());
        System.out.println(clock.toString());
        int h = 0;
        int m = 0;
        int c = 0;
        System.out.print("Введите часы для изменения: ");
        h = sc.nextInt();
        System.out.print("Введите минуты для изменения: ");
        m = sc.nextInt();
        System.out.print("Введите секунды для изменения: ");
        c = sc.nextInt();
        clock.add(h, m, c);
        clock.getTime();
        System.out.println(clock.toString());


    }
}

class Clock {
    private int hour;
    private int minutes;
    private int seconds;

    public Clock(int hour, int minutes, int seconds) {
        this.hour = hour;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        if (hour >= 0 && hour <= 23)
            this.hour = hour;
        else System.out.println("Не правильно заданы часы!");
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if (minutes >= 0 && minutes <= 60)
            this.minutes = minutes;
        else System.out.println("Не правильно заданы минуты!");
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        if (seconds >= 0 && seconds <= 60)
            this.seconds = seconds;
        else System.out.println("Не правильно заданы секунды!");
    }

    @Override
    public String toString() {
        return String.format("Время: %02d:%02d:%02d", hour,minutes,seconds);
    }

    void add(int hour, int minutes, int seconds) {
        this.hour += hour;
        this.minutes += minutes;
        this.seconds += seconds;
    }

    void getTime() {
        long time = getHour() * 60 * 60 + getMinutes() * 60 + getSeconds();
        hour = (int) (time / 60 / 60);
        minutes = (int) (time - (hour * 60 * 60)) / 60;
        seconds = (int) (time - (hour * 60 * 60 + minutes * 60));
        hour = hour % 24;
    }
}