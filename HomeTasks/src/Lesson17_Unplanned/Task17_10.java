package Lesson17_Unplanned;

import java.util.ArrayList;

/*
Класс Абонент: Идентификационный номер, Фамилия, Имя, Отчество, Адрес, Номер
кредитной карточки, Дебет, Кредит, Время междугородных и городских переговоров; Конструктор;
Методы: установка значений атрибутов, получение значений атрибутов, вывод информации.
Создать массив объектов данного класса. Вывести сведения относительно абонентов, у которых
время городских переговоров превышает заданное. Сведения относительно абонентов, которые
пользовались междугородной связью
 */
public class Task17_10 {
    public static void main(String[] args) {
        ArrayList<Subscriber> subList = new ArrayList<>();
        subList.add(new Subscriber(1, "Иванов", "Иван", "Иванович",
                "г. Москва, пр. Ленина д. 23 кв. 5", 808, 20, 0, 100, 20));

        subList.add(new Subscriber(2, "Петров", "Петр", "Петрович",
                "г. Москва, ул. Первая д. 18", 100, 0, 100, 0, 200));

        subList.add(new Subscriber(3, "Фролова", "Софья", "Ивановна",
                "г. Москва, пр. Ленина д. 100 кв. 15", 600, 220, 500, 0, 0));

        System.out.println("Абоненты которые пользовались междугородной связью: ");
        for (Subscriber subscriber : subList) {
            if (subscriber.getMinInter() > 0) {
                System.out.println(subscriber.toString());
            }
        }

        System.out.println("\n Абоненты у которых время городских переговоров превышает 0: ");
        for (Subscriber subscriber : subList) {
            if (subscriber.getMinCity() < 100) {
                System.out.println(subscriber.toString());
            }
        }
    }
}

class Subscriber {
    int id;
    String lastName;
    String name;
    String patronymic;
    String adress;
    int numerCredit;
    double debit;
    double credit;
    int minInter;
    int minCity;

    public Subscriber(int id, String lastName, String name, String patronymic, String adress,
                      int numerCredit, double debit, double credit, int minInter, int minCity) {
        this.id = id;
        this.lastName = lastName;
        this.name = name;
        this.patronymic = patronymic;
        this.adress = adress;
        this.numerCredit = numerCredit;
        this.debit = debit;
        this.credit = credit;
        this.minInter = minInter;
        this.minCity = minCity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public int getNumerCredit() {
        return numerCredit;
    }

    public void setNumerCredit(int numerCredit) {
        this.numerCredit = numerCredit;
    }

    public double getDebit() {
        return debit;
    }

    public void setDebit(double debit) {
        this.debit = debit;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public int getMinInter() {
        return minInter;
    }

    public void setMinInter(int minInter) {
        this.minInter = minInter;
    }

    public int getMinCity() {
        return minCity;
    }

    public void setMinCity(int minCity) {
        this.minCity = minCity;
    }

    @Override
    public String toString() {
        return id + " " + lastName + " " + name + " " + patronymic +
                "\n адрес: " + adress + " \n номер кредитки: " + numerCredit +
                " время междугородных переговоров: " + minInter + " время городских переговоров: " + minCity;
    }
}
