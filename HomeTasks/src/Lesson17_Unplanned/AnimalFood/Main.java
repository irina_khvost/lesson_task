package Lesson17_Unplanned.AnimalFood;
/**
 * Построить три класса (базовый и 3 потомка), описывающих некоторых хищных животных(один из потомков), всеядных(второй потомок)
 * и травоядных (третий потомок). Описать в базовом классе абстрактный метод для расчета количества и типа пищи,
 * необходимого для пропитания животного в зоопарке.
 * a) Упорядочить всю последовательность животных по убыванию количества пищи. При совпадении
 * значений – упорядочивать данные по алфавиту по имени. Вывести идентификатор животного, имя, тип и
 * количество потребляемой пищи для всех элементов списка.
 */

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        ArrayList<Animal> list = new ArrayList<>();
        list.add(new Predator("Wolf", 50));
        list.add(new Herbivores("Cow", 100));
        list.add(new Omnivores("Raccoon", 5));
        list.add(new Predator("Dog", 8));
        list.add(new Omnivores("Bear", 300));
        list.add(new Herbivores("Elephant", 300));

        list.sort(Comparator.reverseOrder());
        for (int i = 0; i < list.size(); i++)  {
            System.out.println(i+1 +" "+ list.get(i));
        }

        System.out.println("Five first names: ");
//b) Вывести первые 5 имен животных из полученного в пункте а) списка.
        for (int i = 0; i < 5; i++) {
            System.out.println(list.get(i));
        }

        System.out.println("Last three ids: ");
//c) Вывести последние 3 идентификатора животных из полученного в пункте а) списка.
        for (int i = list.size() - 1; i > list.size() - 4; i--) {
            System.out.println(i+1 + "  " + list.get(i));
        }

        System.out.println("Save file: Ok!");
//d) Организовать запись и чтение коллекции в/из файл.
// e) Организовать обработку некорректного формата входного файла.
        saveToFile(list);
        System.out.println("Read file: ");
        readToFile();


    }


    static void saveToFile(ArrayList<Animal> list) {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("d:\\Animal.txt"));
            for (Animal r : list) {
                String s = r.toString();
                out.write(s);
                out.newLine();
            }
            out.close();
        } catch (IOException e) {
            System.out.println("IOException save");
        }
    }

    static void readToFile() {
        try {
            BufferedReader inp = new BufferedReader(new FileReader("d:\\Animal.txt"));
            String s;
            while ((s = inp.readLine()) != null) {
                System.out.print(s+ "\r\n");

            }
        } catch (IOException e) {
            System.out.println("IOException read");
        }
    }
}




