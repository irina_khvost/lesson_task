package Lesson17_Unplanned.AnimalFood;

abstract class Animal implements Comparable<Animal> {
    private String name;
    private int weigth;

    public Animal(String name, int weigth) {
        this.name = name;
        this.weigth = weigth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeigth() {
        return weigth;
    }

    public void setWeigth(int weigth) {
        this.weigth = weigth;
    }


    abstract Double food();

    public int compareTo(Animal a) {
        if (food() != a.food()) {
            return food().compareTo(a.food());
        } else
            return name.compareTo(a.name);
    }


    public String toString() {
        return "Name: " + name + " count food: " + food() + " type food: " + getClass().getSimpleName();
    }
}
