package Lesson17_Unplanned.AnimalFood;

class Herbivores extends Animal {

    public Herbivores(String name, int weigth) {
        super(name, weigth);
    }

    @Override
    Double food() {
        return getWeigth() * 0.1;
    }
}
