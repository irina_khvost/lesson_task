package Lesson17_Unplanned.AnimalFood;

class Predator extends Animal {

    public Predator(String name, int weigth) {
        super(name, weigth);
    }

    @Override
    Double food() {
        return getWeigth() * 0.1;
    }

}
