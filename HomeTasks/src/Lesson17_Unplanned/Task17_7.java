package Lesson17_Unplanned;

/*
Создать класс с двумя переменными. Добавить функцию вывода на экран и функцию
изменения этих переменных. Добавить функцию, которая находит сумму значений этих
переменных, и функцию которая находит наибольшее значение из этих двух переменных.
 */
public class Task17_7 {
    public static void main(String[] args) {
        MyClass myclass = new MyClass(5, 7);
        System.out.println(myclass.prit());
        myclass.setA(3);
        myclass.setB(4);
        System.out.println(myclass.prit());
        System.out.println("Сумма a + b = " + myclass.sum());
        System.out.println(myclass.max());
    }
}

class MyClass {
    int a;
    int b;

    public MyClass(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    String prit() {
        return "Переменная a = " + a + " переменная b = " + b;
    }

    int sum() {
        return a + b;
    }

    String max() {
        if (a > b) return a + " больше " + b;
        else return a + " мельше " + b;
    }
}