package Lesson15_Recursion;

/*
Дано натуральное число N. Выведите все его цифры по одной, в обратном порядке, разделяя их
пробелами или новыми строками.
При решении этой задачи нельзя использовать строки, списки, массивы (ну и циклы, разумеется).
Разрешена только рекурсия и целочисленная арифметика.
 */
public class Task15_5 {
    static String s = "";

    public static void main(String[] args) {
        System.out.println(rec(150));
    }

    static String rec(int n) {
        if (n > 0) {
            s = Integer.toString(n % 10);
            return s + " " + rec(n / 10);
        }
        return " ";

    }
}