package Lesson15_Recursion;
/*
Дано слово, состоящее только из строчных латинских букв. Проверьте, является ли это слово
палиндромом. Выведите YES или NO. При решении этой задачи нельзя пользоваться циклами.
 */

public class Task15_8 {
    public static void main(String[] args) {
        System.out.println(rec("LOEEOL"));
    }

    static String rec(String s) {

        if (s.length() == 1) {
            return "YES";
        } else {
            if (s.charAt(0) == s.charAt(s.length() - 1)) {
                if (s.length() == 2) {
                    return "YES";
                }
                return rec(s.substring(1, s.length() - 1));
            } else
                return "NO";
        }
    }


}
