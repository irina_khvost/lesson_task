package Lesson15_Recursion;

/*
В теории вычислимости важную роль играет функция Аккермана A(m,n), определенная следующим
образом: Функция Аккермана
Даны два целых неотрицательных числа m и n, каждое в отдельной строке. Выведите A(m,n).
 */
public class Task15_3 {
    public static void main(String[] args) {
        System.out.println(akkerman(3, 12));
    }

    static int akkerman(int m, int n) {
        int a = 0;
        if (m == 0)
            a = n + 1;
        if (m > 0 && n == 0)
            a = akkerman(m - 1, 1);
        if (m > 0 && n > 0)
            a = akkerman(m - 1, (akkerman(m, n - 1)));
        return a;
    }
}
