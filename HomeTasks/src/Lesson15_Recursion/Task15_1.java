package Lesson15_Recursion;

//Дано натуральное число n. Выведите все числа от 1 до n.
public class Task15_1 {
    public static void main(String[] args) {
        function(10);
    }

    static void function(int n) {
        if (n > 1) {
            function(n - 1);
        }
        System.out.println(n);
    }
}
