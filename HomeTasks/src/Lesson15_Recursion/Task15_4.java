package Lesson15_Recursion;

/*
Дано натуральное число N. Вычислите сумму его цифр.
При решении этой задачи нельзя использовать строки, списки, массивы (ну и циклы, разумеется).
 */
public class Task15_4 {
    static int s = 0;

    public static void main(String[] args) {
        System.out.println(sum(150));
    }

    static int sum(int n) {
        if (n > 0) {
            if (n % 10 != 0) {
                s += n % 10;
            }
            return sum(n / 10);
        }
        return s;
    }
}
