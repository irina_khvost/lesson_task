package Lesson15_Recursion;

import java.util.Scanner;
/*
Дана последовательность натуральных чисел (одно число в строке), завершающаяся числом 0.
Определите наибольшее значение числа в этой последовательности.
В этой задаче нельзя использовать глобальные переменные и передавать какие-либо параметры в
рекурсивную функцию. Функция получает данные, считывая их с клавиатуры. Функция возвращает
единственное значение: максимум считанной последовательности. Гарантируется, что
последовательность содержит хотя бы одно число (кроме нуля).
 */

public class Task15_10 {
    public static void main(String[] args) {
        System.out.println(rec());
    }

    static int rec() {
        Scanner sc = new Scanner(System.in);
        int max = sc.nextInt();
        if (max == 0) {
            return Integer.MIN_VALUE;
        }
        int next = rec();
        return max > next ? max : next;


    }
}
