package Lesson15_Recursion;
//Даны два целых числа A и В (каждое в отдельной строке). Выведите все числа от A до B включительно, в
//порядке возрастания, если A < B, или в порядке убывания в противном случае.

public class Task15_2 {
    public static void main(String[] args) {
        System.out.println(function(5, 2));
        System.out.println(function(1, 6));

    }

    static String function(int a, int b) {
        if (a > b) {
           return a + " " + function(a - 1, b);
        } else {
            if (a == b)
                return Integer.toString(a);
            return a + " " + function(a + 1, b);
        }
    }

}

