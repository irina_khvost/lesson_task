package Lesson15_Recursion;
/*
Дано натуральное число n>1. Выведите все простые множители этого числа в порядке неубывания с
учетом кратности. Алгоритм должен иметь сложность O(logn).
 */

public class Task15_7 {
    public static void main(String[] args) {
        rec(1000, 2);
    }

    static void rec(int n, int k) {
        if (k > n / 2) {
            System.out.print(n + " ");
        } else {
            if (n % k == 0) {
                rec(n / k, k);
                System.out.print(k + " ");

            } else rec(n, k + 1);
        }
    }
}
