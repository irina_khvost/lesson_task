package Lesson1_Loop;

public class Task27 {

    public static void main(String[] args) {
        //Создание двумерного массива
        double[] myArray = {1, -5, 46, 88, -3, 15};
        System.out.println("Наш одномерный массив: ");
        for (double element : myArray) {
            System.out.print(element + " ");
        }
        System.out.println();

        //Вызов наших методов
        System.out.println("Задание 1 (найти мин элемент): " + task1(myArray));
        System.out.println("Задание 2 (найти макс элемент): " + task2(myArray));
        System.out.println("Задание 3 (найти сумму элементов): " + task3(myArray));
        task4(myArray);
        task5(myArray);
        task6(myArray);
        task7(myArray);
        task8(myArray);
        task9(myArray);
        task10(myArray);
        task11(myArray);
        task12(myArray);
        task13(myArray);
        task14(myArray);
        task15(myArray);
        task16(myArray);
        System.out.println("Задание 17 (нормировать все элементы относительно максимального элемента): ");
        task17(myArray);

        //Создание двумерного массива
        double[][] myArrayD = {{1, 5, 9}, {6, 8, -5}, {10, -1, 7}};
        System.out.println("Наш двумерный массив: ");
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                System.out.print(myArrayD[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("Задание 18 (найти минимальный элемент):" + (task18(myArrayD)));
        System.out.println("Задание 19 (найти максимальный элемент):" + (task19(myArrayD)));
        System.out.println("Задание 20 (найти сумму элементов):" + task20(myArrayD));
        System.out.printf("Задание 21 (найти среднее арифметическое): %.3f", task21(myArrayD));
        System.out.println();
        System.out.println("Задание 22 (найти сумму элементов на главной диагонали):" + task22(myArrayD));
        System.out.println("Задание 23 (найти сумму элементов на побочной диагонали):" + task23(myArrayD));
        System.out.println("Задание 24 (найти определитель):" + task24(myArrayD));
        task25(myArrayD);
        System.out.println("Задание 26 (нормировать все элементы):");
        task26(myArrayD);
        System.out.println("Задание 27 (найти определитель):");
        task27(myArrayD);

    }

    //Найти минимальный элемент в одномерном массиве из n элементов.
    static double task1(double[] myArray) {
        double min = myArray[0];
        for (double element : myArray) {
            if (min > element)
                min = element;
        }
        return min;
    }

    //Найти максимальный элемент в одномерном массиве из n элементов.
    static double task2(double[] myArray) {
        double max = myArray[0];
        for (double element : myArray) {
            if (max < element)
                max = element;
        }
        return max;
    }

    //Найти сумму элементов одномерного массива из n элементов.
    static double task3(double[] myArray) {
        double sum = 0;
        for (double element : myArray) {
            sum += element;
        }
        return sum;
    }

    //Найти среднее арифметическое одномерного массива из n элементов.
    static void task4(double[] myArray) {
        double arif = 0.0;
        arif = task3(myArray) / myArray.length;
        System.out.printf("Задание 4 (найти среднее арифметическое): %.3g%n", arif);
    }

    //Найти произведение элементов одномерного массива, состоящего из n элементов
    static void task5(double[] myArray) {
        double mul = 1;
        for (double element : myArray) {
            mul *= element;
        }
        System.out.println("Задание 5 (найти произведение элементов): " + mul);
    }

    //Найти сумму элементов одномерного массива с нечетными номерами, состоящего из n элементов
    static void task6(double[] myArray) {
        double sum = 0;
        for (int i = 1; i < myArray.length; i += 2) {
            sum += myArray[i];
        }
        System.out.println("Задание 6 (найти сумму элементов с нечетными номерами): " + sum);
    }

    //Найти сумму элементов одномерного массива с четными номерами, состоящего из n элементов
    static void task7(double[] myArray) {
        double sum = 0;
        for (int i = 0; i < myArray.length; i += 2) {
            sum += myArray[i];
        }
        System.out.println("Задание 7 (найти сумму элементов с четными номерами): " + sum);
    }

    //Найти произведение элементов одномерного массива с нечетными номерами, состоящего из n элементов
    static void task8(double[] myArray) {
        double mul = 1;
        for (int i = 1; i < myArray.length; i += 2) {
            mul *= myArray[i];
        }
        System.out.println("Задание 8 (найти произведение элементов с нечетными номерами): " + mul);
    }

    //Найти произведение элементов одномерного массива с четными номерами, состоящего из n элементов
    static void task9(double[] myArray) {
        double mul = 1;
        for (int i = 0; i < myArray.length; i += 2) {
            mul *= myArray[i];
        }
        System.out.println("Задание 9 (найти произведение элементов с четными номерами): " + mul);
    }

    //Найти количество элементов, равных нулю, в одномерном массиве, состоящем из n элементов
    static void task10(double[] myArray) {
        int kol = 0;
        for (double element : myArray) {
            if (element == 0) {
                kol++;
            }
        }
        System.out.println("Задание 10 (найти количество элементов, равных нулю): " + kol);
    }

    //Найти количество нечетных элементов в одномерном массиве, состоящем из n элементов
    static void task11(double[] myArray) {
        int kol = 0;
        for (double i = 1; i < myArray.length; i += 2) {
            kol++;
        }
        System.out.println("Задание 11 (найти количество нечетных элементов): " + kol);
    }

    //Найти количество четных элементов в одномерном массиве, состоящем из n элементов
    static void task12(double[] myArray) {
        int kol = 0;
        for (double i = 0; i < myArray.length; i += 2) {
            kol++;
        }
        System.out.println("Задание 12 (найти количество четных элементов): " + kol);
    }

    //Найдите количество элементов, превышающих заданное число k, в одномерном массиве, состоящем из n элементов
    static void task13(double[] myArray) {
        int kol = 0;
        double k = 10.0;
        for (double element : myArray) {
            if (element > k)
                kol++;
        }
        System.out.println("Задание 13 (найдите количество элементов, превышающих " + k + "): " + kol);
    }

    //Найти количество элементов, не превышающих заданное число k, в одномерном массиве, состоящем из n элементов
    static void task14(double[] myArray) {
        int kol = 0;
        double k = 10.0;
        for (double element : myArray) {
            if (element <= k)
                kol++;
        }
        System.out.println("Задание 14 (найдите количество элементов, не превышающих заданного " + k + "): " + kol);
    }

    //Найти количество элементов, больших заданного числа k, в одномерном массиве, состоящем из n элементов
    static void task15(double[] myArray) {
        int kol = 0;
        double k = 10.0;
        for (double element : myArray) {
            if (element > k)
                kol++;
        }
        System.out.println("Задание 15 (найдите количество элементов, больших заданного " + k + "): " + kol);
    }

    //Найти ближайший к нулю или равный ему элемент в одномерном массиве из n элементов
    static void task16(double[] myArray) {
        double minElement = myArray[0];
        for (double element : myArray) {
            if (minElement >= Math.abs(element) - 0)
                minElement = element;
        }
        System.out.println("Задание 16 (найти ближайший к нулю или равный ему элемент): " + minElement);
    }

    //Нормировать все элементы в одномерном массиве из n элементов относительно максимального элемента
    static void task17(double[] myArray) {
        for (int i = 0; i < myArray.length; i++) {
            System.out.print(myArray[i] / task2(myArray) + (i == myArray.length - 1 ? "\n" : "     "));
        }
    }

    //Найти минимальный элемент в двумерном массиве из n элементов
    static double task18(double[][] myArrayD) {
        double min = myArrayD[0][0];
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                if (min >= myArrayD[i][j]) {
                    min = myArrayD[i][j];
                }
            }
        }
        return min;
    }

    //Найти максимальный элемент в двумерном массиве из n элементов.
    static double task19(double[][] myArrayD) {
        double max = myArrayD[0][0];
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                if (max <= myArrayD[i][j]) {
                    max = myArrayD[i][j];
                }
            }
        }
        return max;
    }

    //Найти сумму элементов двумерного массива из n элементов.
    static double task20(double[][] myArrayD) {
        double sum = 0.0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                sum += myArrayD[i][j];
            }
        }
        return sum;
    }

    //Найти среднее арифметическое двумерного массива из n элементов.
    static double task21(double[][] myArrayD) {
        return task20(myArrayD) / (myArrayD.length * myArrayD[0].length);

    }

    //Найти сумму элементов на главной диагонали в квадратном двумерном массиве из n элементов
    static double task22(double[][] myArrayD) {
        double sum = 0.0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                if (i == j)
                    sum += myArrayD[i][j];
            }
        }
        return sum;
    }

    //Найти сумму элементов на побочной диагонали в квадратном двумерном массиве из n элементов
    static double task23(double[][] myArrayD) {
        double sum = 0.0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                if (j == myArrayD[i].length - 1 - i)
                    sum += myArrayD[i][j];
            }
        }
        return sum;
    }

    //Найти определитель квадратного двумерного массива из n элементов.
    static double task24(double[][] myArrayD) {
        double a;
        double b;
        double sum = 0.0;
        for (int j = 0; j < myArrayD.length; j++) {
            a = 1.0;
            b = 1.0;
            for (int i = 0; i < myArrayD[j].length; i++) {
                a *= myArrayD[i][(j + i) % myArrayD.length];
                b *= myArrayD[myArrayD.length - i - 1][(j + i) % myArrayD.length];
            }
            sum += a - b;
        }
        return sum;
    }

    //Найти обратную матрицу для квадратного двумерного массива
    static void task25(double[][] myArrayD) {

    }

    //Нормировать все элементы в двумерном массиве из n элементов относительно максимального элемента.
    static void task26(double[][] myArrayD) {
        double[][] clonArr = new double[myArrayD.length][myArrayD[0].length];
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                clonArr[i][j] = myArrayD[i][j] / task19(myArrayD);
                System.out.print(clonArr[i][j] + "   ");
            }
            System.out.println();
        }
    }

    //Транспонировать квадратный массив из n элементов
    static void task27(double[][] myArrayD) {
        double rez;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = i + 1; j < myArrayD[i].length; j++) {
                rez = myArrayD[i][j];
                myArrayD[i][j] = myArrayD[j][i];
                myArrayD[j][i] = rez;
            }
        }
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                System.out.print(myArrayD[i][j] + "   ");
            }
            System.out.println();
        }
    }


}
