package Lesson1_Loop;

public class Task12 {

    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
        task8();
        task9();
        task10();
        task11();
        task12();
    }

    public static void task1() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = 0; x <= 10; x += 0.01) {
            maxTMP = Math.pow(x, 2);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №1: " + Math.round(maxRes));
    }

    public static void task2() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -15; x <= 15; x += 0.01) {
            maxTMP = Math.pow(x, 2) - x + 3;
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №2: " + Math.round(maxRes));
    }

    public static void task3() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            maxTMP = Math.sin(x) + Math.pow(x, 2);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №3: " + Math.round(maxRes));
    }

    public static void task4() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            maxTMP = Math.cos(x) + Math.pow(x, 2);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №4: " + Math.round(maxRes));
    }

    public static void task5() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -50; x <= 50; x += 0.01) {
            maxTMP = Math.pow(x, 3);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №5: " + Math.round(maxRes));
    }

    public static void task6() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            if (!(x > -1 && x < 1))
                maxTMP = 1 / Math.pow(x, 3);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №6: " + Math.round(maxRes));
    }

    public static void task7() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            if (!(x > -1 && x < 1))
                maxTMP = 1 / Math.pow(x, 3) + Math.pow(x, 3);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №7: " + Math.round(maxRes));
    }

    public static void task8() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            maxTMP = Math.pow(Math.E, x);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №8: " + Math.round(maxRes));
    }

    public static void task9() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -100; x <= 100; x += 0.01) {
            maxTMP = Math.pow(Math.E, Math.sin(x));
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №9: " + Math.rint(100.0 * maxRes) / 100.0);
       /* BigDecimal z1 = new BigDecimal(maxRes);
        System.out.println(z1.setScale(3,RoundingMode.HALF_EVEN));*/
    }

    public static void task10() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -10; x <= 10; x += 0.01) {
            maxTMP = Math.pow(x, Math.E);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №10: " + Math.round(maxRes));
    }

    public static void task11() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -5; x <= 5; x += 0.01) {
            maxTMP = Math.pow(x, Math.E) + x;
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №11: " + Math.round(maxRes));
    }

    public static void task12() {
        double x;
        double maxTMP = 0;
        double maxRes = 0;
        for (x = -5; x <= 5; x += 0.01) {
            maxTMP = Math.pow(x, Math.E) + Math.pow(x, 2);
            if (maxRes < maxTMP) {
                maxRes = maxTMP;
            }
        }
        System.out.println("Задача №12: " + Math.round(maxRes));
    }
}






