package Lesson1_Loop;

import java.util.Arrays;

public class Task5 {
    public static void main(String[] args) {
        myClass mc = new myClass(7, 12);
        mc.print();
        mc.max();
        System.out.println("Сумма " + mc.x1 + " и " + mc.x2 + " равна: " + mc.sum());

        Student[] student = new Student[10];
        // student[0] = new Lesson1_Loop.Student("Petrov", 10,  {4,5,4,5,4});

        for (int i = 0; i < student.length; i++) {
            int[] arr = new int[5];
            for (int j = 0; j < arr.length; j++) {
                arr[j] = (int) (Math.random() * 5) + 1;
            }
            student[i] = new Student("Ivanov" + i, i, arr);
            System.out.println(student[i].print());
        }

        for (int j = 0; j < student.length; j++) {
            for (int i = 0; i < student.length - 1 - j; i++) {
                if (student[i].mean() > student[i + 1].mean()) {
                    Student tmp = student[i];
                    student[i] = student[i + 1];
                    student[i + 1] = tmp;
                }
            }
        }

        for (int j = 0; j < student.length; j++) {
            boolean flag = true;
            for (int i = 0; i < student[j].progress.length; i++) {
                if (student[j].progress[i] < 4) {
                flag = false;
                }
            }
            if (flag == true)
                System.out.println(student[j].printR());
        }
    }
}

/*
Создайте класс с именем student, содержащую поля: фамилия и инициалы, номер
группы, успеваемость (массив из пяти элементов). Создать массив из десяти элементов
такого типа, упорядочить записи по возрастанию среднего балла. Добавить возможность
вывода фамилий и номеров групп студентов, имеющих оценки, равные только 4 или 5.
 */
class Student {
    String FIO;
    int numGrupp;
    int[] progress;

    public Student(String FIO, int numGrupp, int[] progress) {
        this.FIO = FIO;
        this.numGrupp = numGrupp;
        this.progress = progress;
    }

    String print() {
        return "ФИО: " + FIO + ", номер группы: " + numGrupp + ", успеваемость: " + Arrays.toString(progress);
    }

    int mean() {
        int sum = 0;
        for (int i = 0; i < progress.length; i++) {
            sum += progress[i];
        }
        return sum / progress.length;
    }

    void sort(Student[] students) {

    }

    String printR() {
        return "Подходящий студент: " + "ФИО: " + FIO + ", номер группы: " + numGrupp;
    }

}

/*
Создать класс с двумя переменными. Добавить функцию вывода на экран и функцию
изменения этих переменных. Добавить функцию, которая находит сумму значений этих
переменных, и функцию которая находит наибольшее значение из этих двух переменных
 */
class myClass {
    int x1;
    int x2;

    myClass(int x1, int x2) {
        this.x1 = x1;
        this.x2 = x2;
    }

    void print() {
        System.out.println("Переменная 1: " + x1);
        System.out.println("Переменная 2: " + x2);
    }

    void max() {
        if (x1 > x2) {
            System.out.println("Переменная " + x1 + " больше " + x2);
        } else {
            System.out.println("Переменная " + x2 + " больше " + x1);
        }
    }

    int sum() {
        return x1 + x2;
    }
}

/*
Создать класс “Lesson1_Loop.Monster” со строковой переменной для имени, целочисленной переменной
для очков здоровья, двумя вещественными переменными для силы атаки и показателя
защиты
*/
class Monster {
    String name;
    int hp;
    double attack;
    double deferce;

    public Monster(String name, int hp, double attack, double deferce) {
        this.name = name;
        this.hp = hp;
        this.attack = attack;
        this.deferce = deferce;
    }
}

/*
Создать класс “Lesson1_Loop.Human” с двумя строковыми переменными для имени и фамилии,
вещественной переменной для возраста и логической переменной для пола.
 */
class Human {
    String name;
    String lastName;
    double age;
    boolean sex;

    public Human(String name, String lastName, double age, boolean sex) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
    }
}