package Lesson1_Loop;

public class Task9 {

    public static void main(String[] args) {
        //Создаем двумерный массив
        double[][] myArrayD = {{1, 5, 9}, {6, 8, -5}, {10, -1, 7}};
        System.out.println("Наш двумерный массив: ");
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                System.out.print(myArrayD[i][j] + " ");
            }
            System.out.println();
        }

        //Заполнение одномерного массива значениями из двумерного
        double[] arr = new double[myArrayD.length * myArrayD[0].length];
        int ind = 0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                arr[ind] = myArrayD[i][j];
                ind++;
            }
        }

        //Запуск наших методов
        System.out.println("Задание 1 (отсортировать по возрастанию):");
        task1(myArrayD, arr);
        System.out.println("Задание 2 (отсортировать по убыванию):");
        task2(myArrayD, arr);
        System.out.println("Задание 3 (отсортировать столбцы по возрастанию суммы):");
        task3(myArrayD);
        System.out.println("Задание 4 (отсортировать столбцы по убыванию суммы ):");
        task4(myArrayD);
        System.out.println("Задание 5 (отсортировать строки по возрастанию суммы четных элементов):");
        task5(myArrayD);
        System.out.println("Задание 6 (отсортировать строки по убыванию суммы четных элементов):");
        task6(myArrayD);
        task7(myArrayD);
        task8(myArrayD);

        //Создание второй двумерной матрицы для умножения, заполняется рандомом
        int[][] myArrayM = {{5, 10, 3}, {1, 6, -1}, {-4, 1, 7}};
        System.out.println("Наш двумерный массив для умножения: ");
        for (int i = 0; i < myArrayM.length; i++) {
            for (int j = 0; j < myArrayM[i].length; j++) {
                System.out.print(myArrayM[i][j] + " ");
            }
            System.out.println();
        }
        double[] myArrayS = new double[myArrayD.length * myArrayM.length];

        System.out.println("Задание 9 (умножение двух матриц):");
        task9(myArrayD, myArrayM, myArrayS);
    }

    //Отсортировать по возрастанию двумерный массив.
    static void task1(double[][] myArrayD, double[] arr) {
        double rezTmp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    rezTmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = rezTmp;
                }
            }
        }
        print(myArrayD, arr);
    }

    //Отсортировать по убыванию двумерный массив.
    static void task2(double[][] myArrayD, double[] arr) {
        double rezTmp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] < arr[j + 1]) {
                    rezTmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = rezTmp;
                }
            }
        }
        print(myArrayD, arr);
    }

    static void print(double[][] myArrayD, double[] arr) {
        int ind = 0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                myArrayD[i][j] = arr[ind];
                System.out.print(myArrayD[i][j] + "  ");
                ind++;
            }
            System.out.println();
        }
    }

    //Отсортировать столбцы в двумерном массиве по возрастанию суммы элементов в них.
    static void task3(double[][] myArrayD) {
        double sum;
        double[] arr = new double[myArrayD[0].length];
        int[] arrInd = new int[myArrayD[0].length];
        for (int j = 0; j < myArrayD[0].length; j++) {
            sum = 0.0;
            for (int i = 0; i < myArrayD[j].length; i++) {
                sum += myArrayD[i][j];
            }
            arr[j] = sum;
            arrInd[j] = j;
        }
        sort(arr,arrInd,false);
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < arrInd.length; j++) {
                System.out.print(myArrayD[i][arrInd[j]] + "  ");
            }
            System.out.println();
        }
    }

    //Отсортировать столбцы в двумерном массиве по убыванию суммы элементов в них.
    static void task4(double[][] myArrayD) {
        double sum;
        double[] arr = new double[myArrayD[0].length];
        int[] arrInd = new int[myArrayD[0].length];
        for (int j = 0; j < myArrayD[0].length; j++) {
            sum = 0.0;
            for (int i = 0; i < myArrayD[j].length; i++) {
                sum += myArrayD[i][j];
            }
            arr[j] = sum;
            arrInd[j] = j;
        }
        sort(arr,arrInd,true);

        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < arrInd.length; j++) {
                System.out.print(myArrayD[i][arrInd[j]] + "  ");
            }
            System.out.println();
        }
    }

    //Отсортировать строки в двумерном массиве по возрастанию суммы четных элементов в них.
    static void task5(double[][] myArrayD) {
        double sum;

        double[] arr = new double[myArrayD[0].length];
        int[] arrInd = new int[myArrayD[0].length];
        for (int i = 0; i < myArrayD.length; i++) {
            sum = 0.0;
            for (int j = 0; j < myArrayD[i].length; j += 2) {
                sum += myArrayD[i][j];
            }
            arr[i] = sum;
            arrInd[i] = i;
        }
        sort(arr,arrInd,false);

        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < arrInd.length; j++) {
                System.out.print(myArrayD[arrInd[i]][j] + "  ");
            }
            System.out.println();
        }
    }

    static void sort(double[] arr, int[] arrInd, boolean flag) {
        double replace;
        int replaceInd;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (flag == false) {
                    //по возрастанию
                    if (arr[j] > arr[j + 1])
                    {
                        replace = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = replace;
                        replaceInd = arrInd[j];
                        arrInd[j] = arrInd[j + 1];
                        arrInd[j + 1] = replaceInd;
                    }
                } else {
                    //по убыванию
                    if (arr[j] < arr[j + 1])
                    {
                        replace = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = replace;
                        replaceInd = arrInd[j];
                        arrInd[j] = arrInd[j + 1];
                        arrInd[j + 1] = replaceInd;
                    }
                }

            }
        }
    }

    //Отсортировать строки в двумерном массиве по убыванию суммы четных элементов в них.
    static void task6(double[][] myArrayD) {
        double sum;
        double replace;
        int replaceInd;
        double[] arr = new double[myArrayD[0].length];
        int[] arrInd = new int[myArrayD[0].length];
        for (int i = 0; i < myArrayD.length; i++) {
            sum = 0.0;
            for (int j = 0; j < myArrayD[i].length; j += 2) {
                sum += myArrayD[i][j];
            }
            arr[i] = sum;
            arrInd[i] = i;
        }
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] < arr[j + 1]) {
                    replace = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = replace;
                    replaceInd = arrInd[j];
                    arrInd[j] = arrInd[j + 1];
                    arrInd[j + 1] = replaceInd;
                }
            }
        }
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < arrInd.length; j++) {
                System.out.print(myArrayD[arrInd[i]][j] + "  ");
            }
            System.out.println();
        }
    }

    //Найти количество строк, содержащих простые числа, в двумерном массиве.
    static void task7(double[][] myArrayD) {
        int ind = 0;
        for (int i = 0; i < myArrayD.length; i++) {
            for (int j = 0; j < myArrayD[i].length; j++) {
                int col = 2;
                double result = 0.0;
                for (int del = 2; del <= myArrayD[i][j]; del++) {
                    result = myArrayD[i][j] % del;
                    if (result == 0 && del != myArrayD[i][j]) {
                        col++;
                    }
                }
                if (col < 3) {
                    ind++;
                    break;
                }
            }
        }
        System.out.println("Задание 7 (найти количество строк, содержащих простые числа): " + ind);
    }

    //Найти количество столбцов, содержащих простые числа, в двумерном массиве.
    static void task8(double[][] myArrayD) {
        int ind = 0;
        for (int j = 0; j < myArrayD[0].length; j++) {
            for (int i = 0; i < myArrayD.length; i++) {
                int col = 2;
                double result = 0.0;
                for (int del = 2; del <= myArrayD[i][j]; del++) {
                    result = myArrayD[i][j] % del;
                    if (result == 0 && del != myArrayD[i][j]) {
                        col++;
                    }
                }
                if (col < 3) {
                    ind++;
                    break;
                }
            }
        }
        System.out.println("Задание 8 (найти количество столбцов, содержащих простые числа): " + ind);
    }

    //Создать программу для умножения двух квадратных матриц
    static void task9(double[][] myArrayD, int[][] myArrayM, double[] myArrayS) {
        int ind = 0;
        for (int j = 0; j < myArrayD[0].length; j++) {
            for (int i = 0; i < myArrayD.length; i++) {
                int sum = 0;
                for (int k = 0; k < myArrayD.length; k++) {
                    sum += myArrayM[j][k] * myArrayD[k][i];
                }
                myArrayS[ind] = sum;
                ind++;
            }
        }

        print(myArrayD, myArrayS);
    }
}