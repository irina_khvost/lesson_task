package Lesson1_Loop;

import java.util.Arrays;

public class Task34 {
    public static void main(String[] args) {
//Создаем символьный массив, содержащий латинский алфавит
        char[] myArrayChar = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P',
                'Q', 'R', 'S', 'T', 'V', 'X', 'Y', 'Z'};
        System.out.println("Наш одномерный символьный массив: ");
        for (char element : myArrayChar) {
            System.out.print(element + " ");
        }
        System.out.println();

//Создаем одномерный массив и заполняем рандомом
        int[] myArrayInt = new int[10];
        System.out.println("Наш одномерный массив: ");
        for (int i = 0; i < myArrayInt.length; i++) {
            myArrayInt[i] = ((int) (Math.random() * 31 - 15));
            System.out.print(myArrayInt[i] + " ");
        }
        System.out.println();

//Вызываем наши методы
        System.out.println("Задача 2 (найти максимальный элемент):" + task2(myArrayInt));
        System.out.println("Задача 3 (найти минимальный элемент):" + task3(myArrayInt));
        System.out.println("Задача 4 (найти сумму элементов, следующих после элемента, равного 1):" + task4(myArrayInt));

//Создаем двемерный массив 3х3
        int[][] myArrayS = new int[3][3];
        System.out.println("Наш двумерный массив: ");
        for (int i = 0; i < myArrayS.length; i++) {
            for (int j = 0; j < myArrayS[i].length; j++) {
                myArrayS[i][j] = ((int) (Math.random() * 31 - 15));
                System.out.print(myArrayS[i][j] + " ");
            }
            System.out.println();
        }

        //Создаем трехмерный массив
        int[][][] myArrThree = new int[3][2][1];
//Вызываем наши методы
        System.out.println("Задача 5 (найти минимальный элемент на главной диагонали):" + task5(myArrayS));
        System.out.println("Задача 6 (трехмерный массив):");
        task6(myArrThree);
        System.out.println("Задача 7 (одинаковы ли все элементы в массиве):" + task7(myArrayInt));
        System.out.println("Задача 8 (найти сумму всех положительных элементов):" + task8(myArrayInt));
        System.out.println("Задача 9 (проверить, что все элементы в массиве расположены по возрастанию):");
        task9(myArrayInt);
        System.out.println("Задача 10 (найти три самых больших элемента в массиве):");
        task10(myArrayInt);
        System.out.println("Задача 11 (перевернуть массив):");
        task11(myArrayInt);
        System.out.println("Задача 12 (Осуществить циклический сдвиг элементов массива вправо):");
        task12(myArrayInt);
        System.out.println("Задача 13 (найти минимальный элемент):" + task13(myArrayInt));
        System.out.println("Задача 14 (найти максимальный элемент):" + task14(myArrayInt));
        System.out.println("Задача 15 (проверить, все ли элементы в массиве различны):" + task15(myArrayInt));
        System.out.println(task16(myArrayInt));
        System.out.println("Задача 17 (найти количество элементов, равных нулю):" + task17(myArrayInt));
        System.out.println(task18(myArrayInt));
        System.out.println("Задача 19 (Нормировать все элементы относительно суммы положительных эл-в):");
        task19(myArrayInt);
        System.out.println("Задача 20 (найти сумму элементов двумерного массива):" + task20(myArrayS));
        System.out.println("Задача 21 (найти среднее арифметическое двумерного массива):" + task21(myArrayS));
        System.out.println(task22(myArrayS));
        System.out.println("Задача 23 (Нормировать все элементы относительно максимального по модулю элемента):");
        task23(myArrayS);
        System.out.println(" Задача 24 (Поделить на два все элементы двумерного массива, большие 10 по модулю):");
        task24(myArrayS);
        System.out.println("Трехмерная матрица:");
        System.out.println(task25(myArrThree));
        System.out.println("Задание 26 (поменять знак у всех отрицательных элементов трехмерного массива): ");
        task26(myArrThree);

        System.out.println("Задача 34 (Отсортировать по возрастанию xy-плоскости трехмерного массива по\n" +
                "\t    возрастанию среднего арифметического элементов каждой из данных xy-плоскостей):");
        task34(myArrThree);
        System.out.println("Задача 27 (найти среднее арифметическое для всех " +
                "неотрицательных элементов трехмерного массива):");
        System.out.println(task27(myArrThree));
        System.out.println("Задача 29 (Отсортировать по возрастанию xy-плоскости трехмерного " +
                "массива по сумме элементов");
        task29(myArrThree);
        System.out.println("Задание 30 (Последовательно вывести элементы трехмерного массива в строку через пробел.):");
        task30(myArrThree);
        System.out.println();
        System.out.println("Задача 31 (Выписать в двумерный массив элементы xy-плоскости трехмерного массива,\n" +
                " содержащей максимальный элемент в данном трехмерном массиве)");
        task31(myArrThree);
        System.out.println();

        System.out.println("Задача 32 (Выписать в двумерный массив элементы xy-плоскости трехмерного массива,\n" +
                "\t      содержащей максимальный элемент в данном трехмерном массиве.):");
        task32(myArrThree);
        System.out.println("Задача 33 (Вычислить средние арифметические для элементов каждой из xy-плоскостей\n" +
                "\t трехмерного массива и вывести их через запятую):");
        task33(myArrThree);

        System.out.println("Задача 28 (отсортировать по возрастанию элементы трехмерного массива):");
        task28(myArrThree);


    }

    //Найти максимальное значение в целочисленном массиве из n-элементов
    static int task2(int[] myArrayInt) {
        int max = myArrayInt[0];
        for (int i = 1; i < myArrayInt.length; i++) {
            if (max < myArrayInt[i]) {
                max = myArrayInt[i];
            }
        }
        return max;
    }

    //Найти минимальное значение в целочисленном массиве из n-элементов
    static int task3(int[] myArrayInt) {
        int min = myArrayInt[0];
        for (int i = 1; i < myArrayInt.length; i++) {
            if (min > myArrayInt[i]) {
                min = myArrayInt[i];
            }
        }
        return min;
    }

    /*
    В целочисленном массиве из n-элементов найти сумму элементов, следующих
    после элемента, равного единице, вывести эту сумму; если такой элемент
    отсутствует, вывести ноль
    */
    static int task4(int[] myArrayInt) {
        int sum = 0;
        for (int i = 0; i < myArrayInt.length - 1; i++) {
            if (myArrayInt[i] == 1) {
                sum += myArrayInt[i + 1];
            }
        }
        return sum;
    }

    //В произвольной квадратной матрице найти минимальный элемент на главной диагонали
    static int task5(int[][] myArrayS) {
        int min = myArrayS[0][0];
        for (int i = 0; i < myArrayS.length; i++) {
            for (int j = 0; j < myArrayS[i].length; j++) {
                if (i == j & (min > myArrayS[i][j])) {
                    min = myArrayS[i][j];
                }
            }
        }
        return min;
    }

    //Создать и заполнить единицами произвольный трехмерный массив
    static void task6(int[][][] myArrThree) {
        //int[][][] test = new int[one][two][three];
        int i = 0;
        while (i < myArrThree.length) {
            int j = 0;
            while (j < myArrThree[i].length) {
                int k = 0;
                while (k < myArrThree[i][j].length) {
                    myArrThree[i][j][k] = 1;
                    k++;
                }
                j++;
            }
            i++;
        }
        print(myArrThree);
    }

    //Проверить, что все элементы в массиве одинаковы. Вывести “true” если одинаковы и “false” если нет
    static boolean task7(int[] myArrayInt) {
        boolean eqvel = false;
        for (int i = 0; i < myArrayInt.length - 1; i++) {
            if (myArrayInt[i] == myArrayInt[i + 1]) {
                eqvel = true;
            } else {
                eqvel = false;
                break;
            }
        }
        return eqvel;
    }

    //Найти сумму положительных элементов массива
    static int task8(int[] myArrayInt) {
        int sum = 0;
        for (int i = 0; i < myArrayInt.length; i++) {
            if (myArrayInt[i] > 0) {
                sum += myArrayInt[i];
            }
        }
        return sum;
    }

    //Проверить, что все элементы в массиве расположены по возрастанию
    static void task9(int[] myArrayInt) {
        int temp;
        for (int i = 0; i < myArrayInt.length - 1; i++) {
            for (int j = 0; j < myArrayInt.length - 1 - i; j++) {
                if (myArrayInt[j] > myArrayInt[j + 1]) {
                    temp = myArrayInt[j];
                    myArrayInt[j] = myArrayInt[j + 1];
                    myArrayInt[j + 1] = temp;
                }
            }
        }
        for (int i = 0; i < myArrayInt.length; i++) {
            System.out.print(myArrayInt[i] + "  ");
        }
        System.out.println("");

    }

    //Найти три самых больших элемента в массиве. Например, для массива [5,1,4,8,3,5,2,7] ответ будет 8, 7, 5.
    static void task10(int[] myArrayInt) {
        task9(myArrayInt);
        for (int i = myArrayInt.length - 3; i < myArrayInt.length; i++) {
            System.out.print(myArrayInt[i] + "  ");
        }
        System.out.println("");
    }

    //Перевернуть массив. Например, [2, 3, 5] превращается в [5, 3, 2]
    static void task11(int[] myArrayInt) {
        int tmp;
        for (int i = 0; i < myArrayInt.length - 1; i++) {
            for (int j = 0; j < myArrayInt.length - 1 - i; j++) {
                tmp = myArrayInt[j];
                myArrayInt[j] = myArrayInt[j + 1];
                myArrayInt[j + 1] = tmp;
            }
        }

        for (int i = 0; i < myArrayInt.length; i++) {
            System.out.print(myArrayInt[i] + "  ");
        }
        System.out.println("");
    }

    /*
    Осуществить циклический сдвиг элементов массива вправо. Например, [1, 5, 3,7]
    после сдвига будет выглядеть так: [7, 1, 5, 3]
     */
    static void task12(int[] myArrayInt) {
        int tmp;
        for (int i = 0; i < myArrayInt.length - 1; i++) {
            //for (int j = 0; j < myArrayInt.length - 1 - i; j++) {
            tmp = myArrayInt[i];
            myArrayInt[i] = myArrayInt[i + 1];
            myArrayInt[i + 1] = tmp;
            // }
        }

        for (int i = 0; i < myArrayInt.length; i++) {
            System.out.print(myArrayInt[i] + "  ");
        }
        System.out.println("");

    }

    //Найти минимальный элемент в одномерном массиве.
    static int task13(int[] myArrayInt) {
        int min = myArrayInt[0];
        for (int i = 1; i < myArrayInt.length; i++) {
            if (min > myArrayInt[i]) {
                min = myArrayInt[i];
            }
        }
        return min;
    }

    //Найти максимальный элемент в одномерном массиве.
    static int task14(int[] myArrayInt) {
        int max = myArrayInt[0];
        for (int i = 1; i < myArrayInt.length; i++) {
            if (max < myArrayInt[i]) {
                max = myArrayInt[i];
            }
        }
        return max;
    }

    //Проверить, все ли элементы в массиве различны. Вывести “true” или “false” соответственно
    static boolean task15(int[] myArrayInt) {
        boolean rez = false;
        for (int i = 0; i < myArrayInt.length - 1; i++) {
            if (myArrayInt[i] != myArrayInt[i + 1]) {
                rez = true;
                break;
            }
        }
        return rez;
    }

    //Определить количество различных элементов в массиве
    static String task16(int[] myArrayInt) {
        //int[] myArrayInt1 = {1, 6, 8, 5, 5, 5, 3, -1, 8, 8}; //!
        task9(myArrayInt);
        int col = myArrayInt.length;
        for (int i = 0; i < myArrayInt.length - 1; i++) {
            if (myArrayInt[i] == myArrayInt[i + 1]) {
                col--;
            }
        }
        return "Задача 16 (Определить количество различных элементов в массиве):" + col;
    }

    //Найти количество элементов, равных нулю, в одномерном массиве.
    static int task17(int[] myArrayInt) {
        int col = 0;
        for (int i = 0; i < myArrayInt.length; i++) {
            if (myArrayInt[i] == 0) {
                col++;
            }
        }
        return col;
    }

    //Найти количество элементов, меньших заданного числа k, в одномерном массиве
    static String task18(int[] myArrayInt) {
        int col = 0;
        int k = -2;
        for (int i = 0; i < myArrayInt.length; i++) {
            if (myArrayInt[i] < k) {
                col++;
            }
        }
        return "Задача 18 (найти количество элементов, меньших заданного числа \"" + k + "\"):" + col;
    }

    //Нормировать все элементы в одномерном массиве из n-элементов, каждый из
    //которых больше, либо равен нулю, относительно их суммы.
    static void task19(int[] myArrayInt) {
        int sum = 0;
        for (int element : myArrayInt) {
            if (element >= 0) {
                sum += element;
            }
        }
        for (int element : myArrayInt) {
            if (element >= 0) {
                System.out.print((float) element / sum + "  ");
            }
        }
        System.out.println();
    }

    //Найти сумму элементов двумерного массива из n-элементов.
    static int task20(int[][] myArrayS) {
        int sum = 0;
        for (int i = 0; i < myArrayS.length; i++) {
            for (int j = 0; j < myArrayS[i].length; j++) {
                sum += myArrayS[i][j];
            }
        }
        return sum;
    }

    //Найти среднее арифметическое двумерного массива из n-элементов.
    static float task21(int[][] myArrayS) {
        return (float) task20(myArrayS) / (float) (myArrayS.length * myArrayS[0].length);
    }

    // Найти сумму элементов на главной и побочной диагоналях в квадратном двумерном массиве
    static String task22(int[][] myArrayS) {
        int sumG = 0;
        int sumP = 0;
        for (int i = 0; i < myArrayS.length; i++) {
            for (int j = 0; j < myArrayS[i].length; j++) {
                if (i == j)
                    sumG += myArrayS[i][j];
                if (j == myArrayS[i].length - 1 - i)
                    sumP += myArrayS[i][j];
            }
        }

        return "Задача 22 Найти сумму на главной даигонали = " + sumG + " и побочной = " + sumP;
    }

    //Нормировать все элементы квадратного двумерного массива относительно
    //максимального по модулю элемента.
    static void task23(int[][] myArrayS) {
        int max = myArrayS[0][0];
        for (int i = 0; i < myArrayS.length; i++) {
            for (int j = 0; j < myArrayS[i].length; j++) {
                if (Math.abs(myArrayS[i][j]) > max) {
                    max = Math.abs(myArrayS[i][j]);
                }
            }
        }
        for (int i = 0; i < myArrayS.length; i++) {
            for (int j = 0; j < myArrayS[i].length; j++) {
                System.out.print((float) myArrayS[i][j] / max + "  ");
            }
            System.out.println();
        }
    }

    //Поделить на два все элементы двумерного массива, большие 10 по модулю
    static void task24(int[][] myArrayS) {
        double rez = 0.0;
        for (int i = 0; i < myArrayS.length; i++) {
            for (int j = 0; j < myArrayS[i].length; j++) {
                if (Math.abs(myArrayS[i][j]) > 10) {
                    rez = (float) myArrayS[i][j] / 2;
                    System.out.print(rez + "  ");
                } else
                    System.out.print((float) myArrayS[i][j] + "  ");
            }
            System.out.println();
        }
    }

    //Найти сумму элементов трехмерного массива.
    static String task25(int[][][] myArrThree) {
        int sum = 0;
        for (int i = 0; i < myArrThree.length; i++) {
            for (int j = 0; j < myArrThree[i].length; j++) {
                for (int k = 0; k < myArrThree[i][j].length; k++) {
                    myArrThree[i][j][k] = ((int) (Math.random() * 31 - 10));
                    sum += myArrThree[i][j][k];
                    System.out.print(myArrThree[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }

        return "Задание 25 (найти сумму элементов трехмерного массива): " + sum;
    }

    //Поменять знак у всех отрицательных элементов трехмерного массива.
    static void task26(int[][][] myArrThree) {
        for (int i = 0; i < myArrThree.length; i++) {
            for (int j = 0; j < myArrThree[i].length; j++) {
                for (int k = 0; k < myArrThree[i][j].length; k++) {
                    //заполняем в task25
                    //myArrThree[i][j][k] = ((int) (Math.random() * 31 - 10));
                    if (myArrThree[i][j][k] < 0)
                        myArrThree[i][j][k] = Math.abs(myArrThree[i][j][k]);
                    System.out.print(myArrThree[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    //Вычислить среднее арифметическое для всех неотрицательных элементов трехмерного массива
    static String task27(int[][][] myArrThree) {
        int sum = 0;
        int kol = 0;
        for (int i = 0; i < myArrThree.length; i++) {
            for (int j = 0; j < myArrThree[i].length; j++) {
                for (int k = 0; k < myArrThree[i][j].length; k++) {
                    //заполняем в task25
                    //myArrThree[i][j][k] = ((int) (Math.random() * 31 - 10));
                    if (myArrThree[i][j][k] > 0) {
                        sum += myArrThree[i][j][k];
                        kol++;
                    }
                }
            }
        }
        return "среднее арифметическое: " + (float) sum / kol;
    }

    //Отсортировать по возрастанию элементы трехмерного массива.
    static void task28(int[][][] myArrThree) {
        boolean flag = false;
        while (flag != true) {
            flag = true;
            int x = 0;
            int y = 0;
            int z = 0;
            for (int i = 0; i < myArrThree.length; i++) {
                for (int j = 0; j < myArrThree[i].length; j++) {
                    for (int k = 0; k < myArrThree[i][j].length; k++) {
                        if (myArrThree[i][j][k] < myArrThree[x][y][z]) {
                            int tmp = myArrThree[i][j][k];
                            myArrThree[i][j][k] = myArrThree[x][y][z];
                            myArrThree[x][y][z] = tmp;
                            flag = false;
                        }
                        x = i;
                        y = j;
                        z = k;
                    }
                }
            }
        }
        print(myArrThree);
    }

    //Отсортировать по возрастанию xy-плоскости трехмерного массива по сумме элементов в них.
    static void task29(int[][][] myArrThree) {
        for (int j = 0; j < myArrThree.length; j++) {
            for (int i = 0; i < myArrThree.length - 1 - j; i++) {
                int sum = 0;
                int tmp = 0;
                sum += task20(myArrThree[i]);
                tmp += task20(myArrThree[i + 1]);

                int[][] tmparr;
                if (sum > tmp) {
                    tmparr = myArrThree[i];
                    myArrThree[i] = myArrThree[i + 1];
                    myArrThree[i + 1] = tmparr;
                }
            }
        }

        print(myArrThree);
    }

    static void print(int[][][] myArrThree) {
        for (int i = 0; i < myArrThree.length; i++) {
            for (int j = 0; j < myArrThree[i].length; j++) {
                for (int k = 0; k < myArrThree[i][j].length; k++) {
                    System.out.print(myArrThree[i][j][k] + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    //Последовательно вывести элементы трехмерного массива в строку через пробел.
    static void task30(int[][][] myArrThree) {
        for (int i = 0; i < myArrThree.length; i++) {
            for (int j = 0; j < myArrThree[i].length; j++) {
                for (int k = 0; k < myArrThree[i][j].length; k++) {
                    //заполняем в task25
                    //myArrThree[i][j][k] = ((int) (Math.random() * 31 - 10));
                    System.out.print(myArrThree[i][j][k] + "  ");
                }
            }
        }
    }

    //Выписать в двумерный массив элементы xy-плоскости трехмерного массива,
    //содержащей максимальный элемент в данном трехмерном массиве.
    static void task31(int[][][] myArrThree) {
        int max = myArrThree[0][0][0];
        int ind = 0;

        for (int i = 0; i < myArrThree.length; i++) {
            for (int j = 0; j < myArrThree[i].length; j++) {
                if (max < task14(myArrThree[i][j])) {
                    max = task14(myArrThree[i][j]);
                    ind = i;
                }
            }
        }
        for (int i = 0; i < myArrThree[ind].length; i++) {
            System.out.println(Arrays.toString(myArrThree[ind][i]));
        }
    }

    //Последовательно вывести элементы xy-плоскостей трехмерного массива,
    //перед выводом каждой из них указывать z-координату для выводимой плоскости (z
    //= 0, z = 1 и т.д.)
    static void task32(int[][][] myArrThree) {
        for (int i = 0; i < myArrThree.length; i++) {
            System.out.println("z = " + i);
            for (int j = 0; j < myArrThree[i].length; j++) {
                System.out.println(Arrays.toString(myArrThree[i][j]));
            }
        }
    }

    //Вычислить средние арифметические для элементов каждой из xy-плоскостей
    //трехмерного массива и вывести их через запятую
    static void task33(int[][][] myArrThree) {
        double[] tmparr = new double[myArrThree.length];
        for (int i = 0; i < myArrThree.length; i++) {
            tmparr[i] += (double) task20(myArrThree[i]) / (myArrThree[i].length * myArrThree[i][0].length);
        }
        System.out.println(Arrays.toString(tmparr));
    }

    //Отсортировать по возрастанию xy-плоскости трехмерного массива по
    //возрастанию среднего арифметического элементов каждой из данных xy-
    //плоскостей
    static void task34(int[][][] myArrThree) {
        for (int j = 0; j < myArrThree.length; j++) {
            for (int i = 0; i < myArrThree.length - 1 - j; i++) {
                double arif = 0.0;
                double tmp = 0.0;
                arif = (double) task20(myArrThree[i]) / (myArrThree[i].length * myArrThree[i][0].length);
                tmp = (double) task20(myArrThree[i+1]) / (myArrThree[i+1].length * myArrThree[i+1][0].length);
                int[][] tmparr;
                if (arif > tmp) {
                    tmparr = myArrThree[i];
                    myArrThree[i] = myArrThree[i + 1];
                    myArrThree[i + 1] = tmparr;
                }
            }
        }
        print(myArrThree);
    }


}
