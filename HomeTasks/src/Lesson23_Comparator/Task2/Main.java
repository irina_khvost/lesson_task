package Lesson23_Comparator.Task2;

import java.util.TreeSet;

/**
 * Создать класс маг с полями количество здоровье, количество магии и имя. Написать
 * компаратор для сравнения этих двух магов по количеству наносимого урона. Составить список
 * самых сильных магов с использованием treeSet.
 */

public class Main {
    public static void main(String[] args) {
        TreeSet<Magician> treeSet = new TreeSet<>();
        treeSet.add(new Magician(100,20,"Tom10"));
        treeSet.add(new Magician(120,30,"Tom1"));
        treeSet.add(new Magician(70,51,"Tom2"));
        treeSet.add(new Magician(100,30,"Tom3"));
        treeSet.add(new Magician(105,20,"Tom4"));
        treeSet.add(new Magician(108,10,"Tom5"));
        treeSet.add(new Magician(90,50,"Tom6"));
        treeSet.add(new Magician(99,50,"Tom7"));
        treeSet.add(new Magician(102,21,"Tom8"));
        treeSet.add(new Magician(106,33,"Tom9"));

        for (Magician magician : treeSet) {
            System.out.println(magician);
        }

        System.out.println();

        for (Magician magician : treeSet.tailSet(new Magician(100,30,"Tom3"))) {
            System.out.println(magician);
        }
    }
}
