package Lesson23_Comparator.Task2;

/**
 * Создать класс маг с полями количество здоровье, количество магии и имя. Написать
 * компаратор для сравнения этих двух магов по количеству наносимого урона. Составить список
 * самых сильных магов с использованием treeSet.
 */

public final class Magician implements Comparable<Magician> {
    private int hp;
    private int magic;
    private String name;

    public Magician(int hp, int magic, String name) {
        this.hp = hp;
        this.magic = magic;
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public int getMagic() {
        return magic;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Magician o) {
        if (magic != o.getMagic())
            return magic - o.getMagic();
        else if (!name.equals(o.getName()))
            return name.compareTo(o.getName());
        else return hp - o.getHp();
    }

    @Override
    public String toString() {
        return name + " hp: " + hp + " magic: " + magic;
    }
}
