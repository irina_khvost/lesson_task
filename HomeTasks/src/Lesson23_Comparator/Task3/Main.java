package Lesson23_Comparator.Task3;

/**
 * Создать класс рыцарь с полями количество здоровья, количество брони, количеством
 * урона и именем. Провести рыцарский турнир и узнать кто победил используя treeSet, по
 * критериям количество здоровья, количество брони и наносимый урон.
 */

import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        TreeSet<Knight> treeSet = new TreeSet<>();
        treeSet.add(new Knight(100, 20, 37, "Tom10"));
        treeSet.add(new Knight(120, 30, 30, "Tom1"));
        treeSet.add(new Knight(70, 51, 40, "Tom2"));
        treeSet.add(new Knight(100, 30, 25, "Tom3"));
        treeSet.add(new Knight(105, 20, 27, "Tom4"));
        treeSet.add(new Knight(108, 10, 40, "Tom5"));
        treeSet.add(new Knight(90, 50, 38, "Tom6"));
        treeSet.add(new Knight(99, 50, 43, "Tom7"));
        treeSet.add(new Knight(102, 21, 36, "Tom8"));
        treeSet.add(new Knight(106, 33, 41, "Tom9"));

        for (Knight knight : treeSet) {
            System.out.println(knight);
        }

        System.out.println("Побеждает: " + treeSet.last());
    }
}
