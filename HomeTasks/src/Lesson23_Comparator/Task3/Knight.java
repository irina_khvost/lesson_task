package Lesson23_Comparator.Task3;

/**
 * Создать класс рыцарь с полями количество здоровья, количество брони, количеством
 * урона и именем. Провести рыцарский турнир и узнать кто победил используя treeSet, по
 * критериям количество здоровья, количество брони и наносимый урон.
 */

public class Knight implements Comparable<Knight> {
    private int hp;
    private int armor;
    private int damage;
    private String name;

    Knight(int hp, int armor, int dameg, String name) {
        this.hp = hp;
        this.armor = armor;
        this.damage = dameg;
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public int getArmor() {
        return armor;
    }

    public int getDamage() {
        return damage;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Knight o) {
        return (hp + armor + damage) - (o.hp + o.armor + o.damage);
    }

    @Override
    public String toString() {
        return name + " hp: " + hp + " damage: " + damage + " armor: " + armor;
    }
}
