package Lesson23_Comparator.Task1;

import java.util.TreeSet;

/**
 * Создать класс воин с полями количество здоровье, количество урона и имя. Написать
 * компаратор для сравнения этих двух воинов по количеству наносимого урона. Найти самого
 * слабого и самого сильного рыцаря из списка 10 рыцарей используя treeSet.
 */

public class Main {
    public static void main(String[] args) {
        TreeSet<Warrior> treeSet = new TreeSet<>();
        treeSet.add(new Warrior(100,20,"Tom10"));
        treeSet.add(new Warrior(120,30,"Tom1"));
        treeSet.add(new Warrior(70,50,"Tom2"));
        treeSet.add(new Warrior(100,30,"Tom3"));
        treeSet.add(new Warrior(105,20,"Tom4"));
        treeSet.add(new Warrior(108,10,"Tom5"));
        treeSet.add(new Warrior(90,15,"Tom6"));
        treeSet.add(new Warrior(99,17,"Tom7"));
        treeSet.add(new Warrior(102,21,"Tom8"));
        treeSet.add(new Warrior(106,26,"Tom9"));

        for (Warrior warrior : treeSet) {
            System.out.println(warrior);
        }

        System.out.println("Max damage: " + treeSet.last());
        System.out.println("Min damage: " + treeSet.first());
    }
}
