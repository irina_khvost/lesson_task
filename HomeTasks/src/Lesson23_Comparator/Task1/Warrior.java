package Lesson23_Comparator.Task1;

/**
 * Создать класс воин с полями количество здоровье, количество урона и имя. Написать
 * компаратор для сравнения этих двух воинов по количеству наносимого урона. Найти самого
 * слабого и самого сильного рыцаря из списка 10 рыцарей используя treeSet.
 */

class Warrior implements Comparable<Warrior> {
    private int hp;
    private int damage;
    private String name;

    Warrior(int hp, int damage, String name) {
        this.hp = hp;
        this.damage = damage;
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public int getDamage() {
        return damage;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Warrior o) {
        return damage - o.getDamage();
    }

    @Override
    public String toString() {
        return name + " hp: " + hp + " damage: " + damage;
    }
}
