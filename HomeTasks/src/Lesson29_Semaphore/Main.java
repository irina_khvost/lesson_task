package Lesson29_Semaphore;

import java.util.concurrent.Semaphore;

public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(2);
        Philosopher p = new Philosopher("Филосов1", semaphore);
        Philosopher p1 = new Philosopher("Филосов2", semaphore);
        Philosopher p2 = new Philosopher("Филосов3", semaphore);
        Philosopher p3 = new Philosopher("Филосов4", semaphore);
        Philosopher p4 = new Philosopher("Филосов5", semaphore);

        try {
            Thread.sleep(5000);
            p.disable();
            p1.disable();
            p2.disable();
            p3.disable();
            p4.disable();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
