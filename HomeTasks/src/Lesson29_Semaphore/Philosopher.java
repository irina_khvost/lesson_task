package Lesson29_Semaphore;

import java.util.concurrent.Semaphore;

class Philosopher extends Thread {
    private Semaphore semaphore;
    private boolean isActive;

    void disable(){
        isActive = false;
    }


    Philosopher(String name, Semaphore semaphore) {
        super(name);
        this.semaphore = semaphore;
        isActive = true;
        start();
    }

    @Override
    public void run() {
        while (isActive) {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            eat();
            semaphore.release();
        }
        interrupt();
    }

    private void eat() {
        try {
            System.out.println(getName() + " сел за стол");
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(getName() + " встает из-за стола");
    }


}
