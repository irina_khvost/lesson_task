package Lesson10_Extends;

/*
Создать класс “Human” с двумя строковыми переменными для имени и фамилии, вещественной
переменной для возраста и логической переменной для пола. Добавить в класс конструкторы с
параметрами и без них.
 */

public class Human {

    String name;
    String lastName;
    double old;
    boolean sex;

    public Human() {
        this("Ivan", "Ivanov", 30, false);
    }


    public Human(String name) {
        this(name, "Ivanov", 30, false);
        this.name = name;
    }

    public Human(String name, String lastName) {
        this(name, lastName, 30, false);
        this.name = name;
        this.lastName = lastName;

    }

    public Human(String name, String lastName, double old) {

        this(name, lastName, old, false);
        this.name = name;
        this.lastName = lastName;
        this.old = old;
    }

    public Human(String name, String lastName, double old, boolean sex) {
        this.name = name;
        this.lastName = lastName;
        this.old = old;
        this.sex = sex;
    }

    String print() {
        return "Имя: " + name + " Фамилия: " + lastName + " возраст: " + old + " пол: " + sex;
    }
}
