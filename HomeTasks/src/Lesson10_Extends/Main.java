package Lesson10_Extends;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        //Задача №1
        System.out.println("Задача №1");
        Point point = new Point(5, 10);
        System.out.print("Первая координата X = " + point.getX() + " Y = " + point.getY());
        Point point1 = new Point(6, 11);
        System.out.println(" Вторая координата X = " + point1.x + " Y = " + point1.y);
        System.out.printf("Расстояние между точками = %.2f \n", point.distans(point1.x, point1.y));
        System.out.println();

        //Задача №2
        Fraction fraction = new Fraction(1, 3);
        Fraction fraction1 = new Fraction(1, 2);
        Operation operation = new Operation();
        System.out.println("Задача №2: Действие с дробями: " + fraction.print() + " & " + fraction1.print());
        System.out.print("Сумма: " + operation.sum(fraction, fraction1).print());
        System.out.print(" Разность: " + operation.difference(fraction, fraction1).print());
        System.out.print(" Умножение: " + operation.multiple(fraction, fraction1).print());
        System.out.println(" Деление: " + operation.division(fraction, fraction1).print());
        System.out.println();

        //Задача №3
        Triangle triangle = new Triangle(5, 7, 6);
        System.out.println("Задача №3: A = " + triangle.a + " B = " + triangle.b + " C = " + triangle.c);
        System.out.println("Периметр треугольника: " + triangle.perimetr());
        System.out.printf("Площадь треугольника: %.3f \n", triangle.area());
        triangle.angle();
        System.out.println();

        //Задача №4
        System.out.println("Задача №4 ");
        Human human = new Human("Nick", "Petrov");
        System.out.println(human.print());
        System.out.println();

        //Задача №5
        System.out.print("Задача №5 ");
        Monster monster = new Monster("Nick");
        System.out.println(monster.print());
        System.out.println();

        //Задача №6
        System.out.print("Задача №6 ");
        Function function = new Function();
        System.out.print(function.print());
        System.out.println(function.raschet(2));
        System.out.println();

        //Задача №7
        System.out.print("Задача №7 ");
        Figure figure = new Figure(5, 7, 3);
        System.out.println(figure.print());
        System.out.println();

        //Задача №8
        System.out.println("Задача №8 ");
        Polynomial polynomial = new Polynomial(5, 7, 3, 4, 9, 12);
        System.out.println(polynomial.raschet(2));
        System.out.print(polynomial.printP());
        System.out.println(polynomial.proizvodnaya(2));
        System.out.println();

        //Задача №9
        System.out.println("Задача №9");
        DateM date = new DateM(15, 11, 2001);
        DateM date1 = new DateM(15, 11, 2000);

        System.out.println("Дата:" + date.print());
        System.out.println("Дата:" + date1.print());
        System.out.println("Пора года: " + date.pora());
        System.out.println("Номер дня в году: " + date.day());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String s = date.print();
        String s1 = date1.print();

        Date date11 = dateFormat.parse(s, new ParsePosition(0));
        Date date2 = dateFormat.parse(s1, new ParsePosition(0));
        System.out.println("Разница между датами: " + date.difference(date11, date2));
    }
}

