package Lesson10_Extends;

import java.util.Date;

/*
Создать класс “Date” с тремя целочисленными показателями для определения дня, месяца и года.
Добавить в класс функции для определения по дате времени года, номера дня в году, а также функцию
для расчета количества дней между двумя датами, принимающую на вход объект данного класса.
 */
public class DateM {
    int dd;
    int mm;
    int yyyy;

    public DateM(int dd, int mm, int yyyy) {
        this.dd = dd;
        this.mm = mm;
        this.yyyy = yyyy;
    }

    String pora() {
        String sezon;
        if (mm == 12 || mm == 1 || mm == 2) {
            sezon = "Winter";
        } else {
            if (mm == 3 || mm == 4 || mm == 5) {
                sezon = "Spring";
            } else {
                if (mm == 6 || mm == 7 || mm == 8) {
                    sezon = "Summer";
                } else {
                    if (mm == 9 || mm == 10 || mm == 11) {
                        sezon = "Fall";
                    } else sezon = "ERROR";
                }
            }
        }
        return  sezon;
    }

    int day() {
        int rez = 0;
        int[] arr = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        for (int i = 1; i < mm; i++) {
            rez += arr[i];
        }
        if (mm > 2) {
            if ((yyyy % 4 == 0) && ((yyyy % 100 != 0) || (yyyy % 400 == 0))) {
                rez += 1;
            }
        }
        return rez += dd;
    }

    int difference(Date date, Date date1) {
        int days;
        long milliseconds = date1.getTime() - date.getTime();
       // if (date1.getTime() < date.getTime())
        days = (int) (milliseconds / (24 * 60 * 60 * 1000));
       // else days = (int) (milliseconds / (24 * 60 * 60 * 1000));
        return  Math.abs(days);
    }
    /*
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            Date date1 = dateFormat.parse("15.05.2018");
            Date date2 = dateFormat.parse("17.05.2018");

            System.out.println("Первая дата: " + date1);
            System.out.println("Вторая дата: " + date2);

            long milliseconds = date2.getTime() - date1.getTime();
            System.out.println("\nРазница между датами в миллисекундах: " + milliseconds);

            // 24 часа = 1 440 минут = 1 день
            int days = (int) (milliseconds / (24 * 60 * 60 * 1000));
            System.out.println("Разница между датами в днях: " + days);
     */


    String print() {
        return dd + "." + mm + "." + yyyy;
    }

}
