package Lesson10_Extends;

/*
Создать класс “Point” с двумя вещественными переменными для определения координат точки по x и
y. Добавить в класс конструктор с параметрами, две функции для возвращения значений координат x и y
соответственно, а также функцию, в которую передаются координаты другой точки, после чего в ней
рассчитывается расстояние до нее и выводится на экран.
*/

class Point {
    double x;
    double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    double distans(double x, double y) {
        return Math.sqrt((Math.pow(x - this.x, 2)) + (Math.pow(y - this.y, 2)));
    }

}