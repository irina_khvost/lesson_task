package Lesson10_Extends;

/*
Создать класс ”Function” c четырьмя вещественными показателями для коэффициентов при каждом
члене в формуле x3+x2+x+d, также создать функцию, принимающую на вход показатель x и выводящую
значение функции в соответствующей x точке. Добавить в класс конструктор с параметрами, функцию для
вывода формулы с добавленными в нее параметрами.
 */
public class Function {
    double a;
    double b;
    double c;
    double d;


    public Function() {
        this.a = 5;
        this.b = 3;
        this.c = 2;
        this.d = 5;
    }

    public Function(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    double raschet(int x) {
        return (a * Math.pow(x, 3)) + (b * Math.pow(x, 2)) + (c * x) + d;
    }

    String print() {
        return "Формула: " + a + "*x^3 + " + b + "*x^2 + " + c +"*x + " + d + " = ";
    }


}
