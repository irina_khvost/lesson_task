package Lesson10_Extends;

/*
Создать класс “Monster” со строковой переменной для имени, целочисленной переменной для очков
здоровья, двумя вещественными переменными для силы атаки и показателя защиты. Добавить в класс
конструктор с параметрами и функцию для вывода всех параметров, на экран.
 */
public class Monster {
    String name;
    int hp;
    double attack;
    double defence;

    public Monster(String name, int hp, double attack, double defence) {
        this.name = name;
        this.hp = hp;
        this.attack = attack;
        this.defence = defence;
    }

    public Monster(String name) {
        this(name, 100, 3.5, 20.5);
        this.name = name;
    }

    public Monster(String name, int hp) {
        this(name, hp, 3.5, 20.5);
        this.name = name;
        this.hp = hp;
    }

    public Monster(String name, int hp, double attack) {
        this(name, hp, attack, 20.5);
        this.name = name;
        this.hp = hp;
        this.attack = attack;
    }

    String print() {
        return "Имя: " + name + " очки здоровья: " + hp + " атака: " + attack + " защита: " + defence;
    }

}
