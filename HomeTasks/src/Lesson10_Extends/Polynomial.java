package Lesson10_Extends;
/*
Создать класс “Palindrome” с шестью вещественными показателями для коэффициентов при каждом
члене в формуле ax5+bx4+cx3+dx2+ex+k. Добавить в класс функции для расчета значения полинома в
указанной точке x, а также значения производной, также в указанной точке x.
 */

class Polynomial{
    double a;
    double b;
    double c;
    double d;
    double e;
    double k;

    public Polynomial(double a, double b, double c, double d, double e, double k) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.k = k;
    }

    String raschet(int x) {
        double ras = (a * Math.pow(x, 5)) + (b * Math.pow(x, 4)) + (c * Math.pow(x, 3)) +(d * Math.pow(x, 2)) + k;
        return "Формула: " + a + "*x^5 + " + b + "*x^4 + " + c +"*x^3 + " + d + "*x^2 + " + k + "= " + ras;
    }

    double proizvodnaya(int x) {
       return (5 * Math.pow(x, 4)) + (4 * Math.pow(x, 3)) + (3 * Math.pow(x, 2)) +(2 * Math.pow(x, 1)) + (1 * Math.pow(x, 0) + k);
    }

    String printP() {
        return "Формула для производной: 5*x^4 + 4*x^3 + 3*x^2 + 2*x^1 + 1*x^0 + " + k + "= ";
    }
}
