package Lesson10_Extends;

/*
Создать класс “Figure” с тремя вещественными показателями для длины, ширины и высоты фигуры.
Создать три конструктора, которые принимают от одного до трех указанных выше параметров для
создания объектов класса, представляющих соответственно прямую, прямоугольник и параллелепипед.
Добавить в класс функцию для вывода типа фигуры в соответствии с определенными в конструкторе
параметрами.
 */
public class Figure {
    double dlina;
    double shirina;
    double visota;
    String type;


    public Figure() {
    }

    public Figure(double dlina, double shirina, double visota) {
        this.dlina = dlina;
        this.shirina = shirina;
        this.visota = visota;
        this.type = "параллелепипед";
    }

    public Figure(double dlina) {
        this(dlina, 0, 0);
        this.dlina = dlina;
        this.type = "прямая";
    }

    public Figure(double dlina, double shirina) {
        this(dlina, shirina, 0);
        this.dlina = dlina;
        this.shirina = shirina;
        this.type = "прямоугольник";
    }

    String print() {
        return "Длина " + dlina + " Ширина " + shirina + " Высота " + visota + " Тип фигуры: " + type;
    }

}
