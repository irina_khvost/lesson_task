package Lesson10_Extends;

/*
Создать класс “Triangle” с тремя вещественными переменными для задания длин его сторон.
Добавить в класс функции для расчета периметра, площади по формуле Герона, а также для определения
типа треугольника.
 */
public class Triangle {
    double a;
    double b;
    double c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    double perimetr() {
        return a + b + c;
    }

    double area() {
        return Math.sqrt((perimetr() / 2) * (perimetr() / 2 - a) * (perimetr() / 2 - b) * (perimetr() / 2 - c));
    }

    void angle() {
        if (Math.pow(a, 2) + Math.pow(b, 2) > Math.pow(c, 2))
            System.out.println("Треугольник с острым углом");
        if (Math.pow(a, 2) + Math.pow(b, 2) < Math.pow(c, 2))
            System.out.println("Треугольник с тупым углом");
        if (Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2))
            System.out.println("Треугольник с прямым углом");

    }
}


