package Lesson16_Exception;

import java.util.Scanner;

public class Task16_4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число для подсчета 12*x +44: ");
        int s = sc.nextInt();
        try {
        if (s < -71 || s > 14) throw new FunctionException("Введено значение которое выходит за пределы. ");
        int a = 12 * s + 44;
        System.out.println(a);
         } catch (FunctionException ex) {
             System.out.println(ex.getMessage() + "Это: " + s);
        }
    }

    static class FunctionException extends Exception {
        public FunctionException(String mes) {
            super(mes);
        }
    }
}
