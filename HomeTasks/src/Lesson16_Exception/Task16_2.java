package Lesson16_Exception;

import java.util.Scanner;

/*
Написать программу, которая будет считать значение функции f(x) = 12*x + 44 на
промежутке [-71; 14]. Значение при котором нужно посчитать значение функции вводится с
клавиатуры. Если оно не входит в заданный промежуток - бросается исключение. Программа при
этом не должна завершать своей работы.
 */
public class Task16_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число для подсчета 12*x +44: ");
        int s = sc.nextInt();
        try {
            if (s < -71 || s > 14) throw new Exception("Введено значение которое выходит за пределы. ");
            int a = 12 * s + 44;
            System.out.println(a);
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "Это: " + s);
        }

    }
}
