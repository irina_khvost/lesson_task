package Lesson16_Exception;

import java.util.Scanner;

/*
Написать программу, которая будет считать значение функции f(x) = 1 / x при заданных
значениях. Программа может принимать на вход любые значения отличные от нуля. Поймать в ней
случай деления на ноль с помощью exeption’ов так, чтобы программа не завершала свою работу.
 */
public class Task16_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число для подсчета 1/х: ");
        int s = sc.nextInt();
        try {
            if (s == 0.0) throw new Exception("Введено значение на которое делить нельзя. ");
            double a = 1 / (double) s;
            System.out.println(a);
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "Это: " + s);
        }
    }


}
