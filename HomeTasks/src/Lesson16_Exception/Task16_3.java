package Lesson16_Exception;
/*
Написать программу, которая будет считать значение функции f(x) = 18*x ^ 2 + (54/x) - 8 на заданном
промежутке ( промежуток вводится с клавиатуры ). Далее вводится с клавиатуры значение при котором нужно посчитать
функцию. Если значение не входит в заданный промежуток - бросается исключение. Программа при этом не должна
завершать своей работы.
 */

import java.util.Scanner;

public class Task16_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите промежуток (начало): ");
        int a = sc.nextInt();
        System.out.print("Введите промежуток (конец): ");
        int b = sc.nextInt();
        System.out.print("Введите число для подсчета 18*x^2 + (54/x) - 8: ");
        int s = sc.nextInt();
        try {
            if (s < a || s > b) throw new Exception("Введено значение которое выходит за пределы. ");
            int k = (int) Math.pow(18 * s, 2) + (54 / s) - 8;
            System.out.println(k);
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "Это: " + s);
        }

    }

}
