package Lesson24_Iterator.Task1;

import java.util.Map;
import java.util.TreeMap;

/**
 * Рыцарский турнир закончился! Пришла пора объявить победителей. Но безответственный
 * клерк перепутал имена участников и теперь не может составить их правильный список. Нужно ему
 * помочь. А именно, используя treeMap создать список участников с полями “Честь” и соответствующее
 * ему “Имя рыцаря” и показать отсортированный список. Помним, чем больше чести, тем ближе к
 * верхушке турнирной таблицы.
 */

public class Main {
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        treeMap.put(3, "Ivan");
        treeMap.put(4, "Petr");
        treeMap.put(1, "Ekaterina");
        treeMap.put(5, "Salomon");
        treeMap.put(2, "Karl");

        for (Map.Entry<Integer, String> element : treeMap.entrySet()) {
            System.out.println(element.getKey() + "  " + element.getValue());
        }
    }
}
