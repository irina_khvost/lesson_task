package Lesson24_Iterator.Task2;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * В средневековом городе живёт король (king), 10 дворян (nobles), 25 рыцарей (knights) и 100 обычных крестьян (peasants). В
 * город пришла чума. Каждый день все жители этого города теряют часть своего здоровья. Определить
 * сколько и каких жителей останется в городе после 5 дней чумы.
 * Нужно: Создать 4 класса - король, дворяне, рыцари, крестьяне. Чем выше сословие, тем более здоровый человек
 * Добавить их в ArrayList. “Каждый день” у жителей отнимается случайное количество здоровья.
 * В задаче с чумой добавить использование буферизированного потока
 * (BufferedInputStream / BufferedOutputStream) для записи и чтения из файла
 */

public class Main {
    public static void main(String[] args) {
        ArrayList<Person> people = createPeople();
        Plague.dead(people);
        Plague.statistic(people);
        writeFile(people);
        people.forEach(System.out::println);
    }

    static ArrayList<Person> createPeople() {
        ArrayList<Person> people = new ArrayList<>();
        Random rnd = new Random();
        ArrayList<String> names = namesRead();
        people.add(new King(100, names.get(rnd.nextInt(names.size()))));
        for (int i = 0; i < 10; i++) {
            people.add(new Noble(80, names.get(rnd.nextInt(names.size())) + i));
        }
        for (int i = 0; i < 25; i++) {
            people.add(new Knight(60, names.get(rnd.nextInt(names.size())) + i));
        }
        for (int i = 0; i < 100; i++) {
            people.add(new Peasant(20, names.get(rnd.nextInt(names.size())) + i));
        }
        return people;
    }

 /*   static ArrayList<String> namesRead() {
        String path = "C:/Users/Sacinandan Kisor das/IdeaProjects/HomeTasks/src/Lesson24_Iterator/name.txt";
        ArrayList<String> names = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(path)) {
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer, 0, buffer.length);
            String str = "";
            for (int i = 0; i < buffer.length; i++) {
                str = str + (char) buffer[i];
            }
            names.addAll(Arrays.asList(str.split("\r\n")));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return names;
    }*/

    static ArrayList<String> namesRead() {
        String path = "C:/Users/Sacinandan Kisor das/IdeaProjects/HomeTasks/src/Lesson24_Iterator/name.txt";
        ArrayList<String> names = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream(path);
             BufferedInputStream bis = new BufferedInputStream(fis)) {
            int c;
            String str = "";
            while ((c = bis.read()) != -1) {
                str = str + (char) c;
            }
            names.addAll(Arrays.asList(str.split("\r\n")));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return names;
    }

    /* static void writeFile(ArrayList<Person> people) {
         String path = "C:/Users/Sacinandan Kisor das/IdeaProjects/HomeTasks/src/Lesson24_Iterator/statistic.txt";
         try (FileOutputStream fos = new FileOutputStream(path)) {
             for (Person person : people) {
                 String tmp = person.toString();
                 byte[] buffer = tmp.getBytes();
                 fos.write(buffer, 0, buffer.length);
                 fos.write("\r\n".getBytes());
             }
         } catch (IOException e) {
             System.out.println(e.getMessage());
         }
     }*/
    static void writeFile(ArrayList<Person> people) {
        String path = "C:/Users/Sacinandan Kisor das/IdeaProjects/HomeTasks/src/Lesson24_Iterator/statistic.txt";
        try (FileOutputStream fos = new FileOutputStream(path);
             BufferedOutputStream bos = new BufferedOutputStream(fos)) {
            for (Person person : people) {
                bos.write(person.toString().getBytes());
                bos.write("\r\n".getBytes());
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
