package Lesson24_Iterator.Task2;

import java.util.ArrayList;

public class Plague {
    static void dead(ArrayList<Person> people) {
        for (Person person : people) {
            for (int i = 0; i < 5; i++) {
                person.setHp(person.getHp() - (int) (Math.random() * 35) + 1);
            }
        }
        people.removeIf(person -> person.getHp() <= 0);
    }

    static void statistic(ArrayList<Person> people) {
        int countNobles = 0;
        int countKnights = 0;
        int countPeasants = 0;

        for (Person person : people) {
            if (person instanceof Noble) {
                countNobles++;
            } else if (person instanceof Knight) {
                countKnights++;
            } else countPeasants++;
        }

        if (people.get(0) instanceof King) System.out.println("король остался в живых!");
        else System.out.println("король мертв!");
        System.out.println("дворян: " + countNobles);
        System.out.println("рыцарей: " + countKnights);
        System.out.println("крестьян: " + countPeasants);
        System.out.println();
    }
}
