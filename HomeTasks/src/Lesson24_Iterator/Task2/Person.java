package Lesson24_Iterator.Task2;

public abstract class Person {
    private int hp;
    private String name;

    public Person(int hp, String name) {
        this.hp = hp;
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public String getName() {
        return name;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Name: %9s \tHP: %3s \tClass: %7s", name, hp, getClass().getSimpleName());
    }
}
