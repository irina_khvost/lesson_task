package Lesson27_ClassFile;

/**
 * Создать класс студент с полями фамилия, имя, массив с оценками и поле со средним
 * значением оценки. Используя сериализацию записать в файл данные о 10 студентах и
 * впоследствии считать из файла и вывести информацию о них.
 * Модифицировать прошлую задачу, исключив поле средней оценки из сериализованных данных.
 */

import java.io.*;
import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        ArrayList<Student> student = creatStudent();
        writeFile(student);
        readFile().forEach(System.out::println);
    }

    private static ArrayList<Student> creatStudent(){
        ArrayList<Student> student = new ArrayList<>();
        Random rnd = new Random();
        for (int i = 0; i < 10; i++) {
            ArrayList<Integer> tmp = new ArrayList<>();
            for (int j = 0; j < 5; j++) {
                tmp.add(rnd.nextInt(5)+1);
            }
            student.add(new Student("Ivanov"+i, "Ivan"+i,tmp));
        }
        return student;
    }

    private static void writeFile(ArrayList<Student> students){
        String path = "C:\\Users\\Sacinandan Kisor das\\IdeaProjects\\HomeTasks\\src\\Lesson27_ClassFile\\student.dat";
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
            oos.writeObject(students);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static ArrayList<Student> readFile(){
        ArrayList<Student> newStudent = new ArrayList<>();
        String path = "C:\\Users\\Sacinandan Kisor das\\IdeaProjects\\HomeTasks\\src\\Lesson27_ClassFile\\student.dat";
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path))){
            newStudent = ((ArrayList<Student>) ois.readObject());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return newStudent;
    }
}
