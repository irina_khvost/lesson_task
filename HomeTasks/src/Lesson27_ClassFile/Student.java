package Lesson27_ClassFile;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Создать класс студент с полями фамилия, имя, массив с оценками и поле со средним
 * значением оценки. Используя сериализацию записать в файл данные о 10 студентах и
 * впоследствии считать из файла и вывести информацию о них.
 * Модифицировать прошлую задачу, исключив поле средней оценки из сериализованных данных.
 */

public class Student implements Serializable {
    private String lastName;
    private String name;
    private ArrayList<Integer> grade;
    private /*transient*/ double avg;


    public Student(String lastName, String name, ArrayList<Integer> grade) {
        this.lastName = lastName;
        this.name = name;
        this.grade = grade;
        int sum = 0;
        for (Integer list : grade) {
            sum = sum + list;
        }
        this.avg = (double) sum/grade.size();
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Integer> getGrade() {
        return grade;
    }

    public void setGrade(ArrayList<Integer> grade) {
        this.grade = grade;
    }

    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    @Override
    public String toString() {
        return String.format("LastName: %8s \tName: %6s \tGrade: %4s \t Avg: %.1f", lastName, name, grade,avg);
    }
}
