package RPSPlay;
public interface Player {

    String getName(); //возвращает имя игрока
    int getMove(); //возвращает решение игрока на текущий ход
    void addPoint();//добавляет одно очко игроку, когда он выигрывает
    int getPoints();//возвращает количество очков игрока




}
