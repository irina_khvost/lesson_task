package RPSPlay;

import java.util.Scanner;

public class Game {
    private Human humanG;
    private Computer computerG;

    public Game() {
        // this.humanG = humanG;
        //this.computerG = computerG;
    }

    void play(Human humanG, Computer computerG) {
        Scanner sc = new Scanner(System.in);
        boolean flag = true;
        System.out.print("Хотите ли вы играть? (1 - да/0 - нет): ");
        if (sc.nextInt() == 0) flag = false;

        while (flag) {

            System.out.print("Введите ваш ход (1 - камень, 2 - ножницы, 3 - бумага):");
            int s = sc.nextInt();
            try {
                if (s < 1 || s > 3) throw new Exception("Введено значение которое выходит за пределы. ");
                humanG.setMove(s);
                computerG.setMove();
                System.out.print("Ход компьютера: " + computerG.getMove());
                System.out.println();
                if (humanG.getMove() == computerG.getMove()) {
                    System.out.println("Ничья...");
                } else if (humanG.getMove() == 1 && computerG.getMove() == 2) {
                    humanG.addPoint();
                    System.out.println("Выйграл " + humanG.getName());
                } else if (humanG.getMove() == 2 && computerG.getMove() == 1) {
                    computerG.addPoint();
                    System.out.println("Выйграл " + computerG.getName());
                } else if (humanG.getMove() == 1 && computerG.getMove() == 3) {
                    computerG.addPoint();
                    System.out.println("Выйграл " + computerG.getName());
                } else if (humanG.getMove() == 3 && computerG.getMove() == 1) {
                    humanG.addPoint();
                    System.out.println("Выйграл " + humanG.getName());
                } else if (humanG.getMove() == 2 && computerG.getMove() == 3) {
                    computerG.addPoint();
                    System.out.println("Выйграл " + computerG.getName());
                } else if (humanG.getMove() == 3 && computerG.getMove() == 2) {
                    humanG.addPoint();
                    System.out.println("Выйграл " + humanG.getName());
                }
                System.out.println("Количество очков " + humanG.getName() + " = " + humanG.getPoints() +
                        ", у " + computerG.getName() + " = " + computerG.getPoints());
                System.out.println("Хотите ли вы продолжить? 1 - да/ 0 - нет");
                if (sc.nextInt() == 0) flag = false;
            } catch (Exception ex) {
                System.out.println(ex.getMessage() + "Это: " + s);
                System.out.println("Хотите ли вы продолжить? 1 - да/ 0 - нет");
                if (sc.nextInt() == 0) flag = false;
            }
        }
    }
}
