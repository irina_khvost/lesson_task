package RPSPlay;

public class Computer implements Player {
    private String name = "Computer";
    private int move = 0;
    private int points = 0;

    public Computer() {
        this.name = name;
        this.move = move;
        this.points = points;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMove() {
        this.move = (int) (Math.random() * 3) + 1;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String getName() {
        return name;

    }

    @Override
    public int getMove() {
        return move;
    }

    @Override
    public void addPoint() {
        points++;
    }

    @Override
    public int getPoints() {
        return points;
    }
}
