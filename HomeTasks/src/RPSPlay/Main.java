package RPSPlay;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Human human = new Human();
        Computer computer = new Computer();
        Game game = new Game();
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите имя игрока: ");
        human.setName(sc.next());
        game.play(human, computer);

    }
}

