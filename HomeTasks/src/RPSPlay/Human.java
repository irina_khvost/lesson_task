package RPSPlay;

public class Human implements Player {
    private String name = "";
    private int move = 0;
    private int points = 0;

    public Human() {
        this.name = name;
        this.move = move;
        this.points = points;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMove(int move) {
        this.move = move;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getMove() {
        return move;
    }

    @Override
    public void addPoint() {
        points++;
    }

    @Override
    public int getPoints() {
        return points;

    }
}
