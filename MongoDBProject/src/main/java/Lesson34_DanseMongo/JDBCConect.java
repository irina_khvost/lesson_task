package Lesson34_DanseMongo;

import java.awt.*;
import java.sql.*;
import java.util.ArrayList;

public class JDBCConect {
    final static String DB_DRIVER = "com.mysql.jdbc.Driver";
    final static String DB_CONNECTION = "jdbc:mysql://localhost:3306/test?" +
            "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    final static String DB_USER = "root";
    final static String DB_PASSWORD = "";

    private static Connection getDBConnection() {
        Connection dbConection = null;
        try {
            Class.forName(DB_DRIVER);
            dbConection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            return dbConection;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dbConection;
    }

    public void createTableSQL() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;

        String createTableSQL = "CREATE TABLE `table1` (`_id` INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, " +
                "`name` VARCHAR (100), `x` int NULL, `y` int NULL);";
        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();

            // выполнить SQL запрос
            statement.execute(createTableSQL);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }

    public void addVal(Human human) throws SQLException {
        String sqlInsert = "INSERT INTO `table1` (`name`, `x`, `y` )\n" +
                "VALUES ('" + human.getName() + "', " + human.getPoint().x + ", " + human.getPoint().y + " );";
        Connection dbConnection = null;
        Statement statement = null;
        dbConnection = getDBConnection();
        try {
            statement = dbConnection.createStatement();
            statement.execute(sqlInsert);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }

    public ArrayList<Human> getAllData() throws SQLException {
        String sqlSelect = "SELECT * FROM `table1` \n" +
                "ORDER BY name;";
        Main.humans = new ArrayList<Human>();
        Connection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();
            ResultSet rs = statement.executeQuery(sqlSelect);

            while (rs.next()) {
                String name = rs.getString("name");
                int x = rs.getInt("x");
                int y = rs.getInt("y");
                Main.humans.add(new Graph(name, new Point(x, y)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return Main.humans;
    }

    public void delTable() throws SQLException {
        String sqlDrop = "DROP TABLE `table1`;";
        Connection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = getDBConnection();
            statement = dbConnection.createStatement();
            int dp = statement.executeUpdate(sqlDrop);
            createTableSQL();
        } catch (SQLException e) {
            createTableSQL();
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }

}

