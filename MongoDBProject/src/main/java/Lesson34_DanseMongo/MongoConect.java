package Lesson34_DanseMongo;

import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.Properties;

public class MongoConect {
    // это клиент который обеспечит подключение к БД
    private MongoClient mongoClient;

    // В нашем случае, этот класс дает возможность аутентифицироваться в MongoDB
    private DB db;

    // тут мы будем хранить состояние подключения к БД
    private boolean authenticate;

    // И класс который обеспечит возможность работать с коллекциями/таблицами MongoDB
    private DBCollection table;

    Properties prop;

    public MongoConect(){
        prop = new Properties();
        prop.setProperty("host", "localhost");
        prop.setProperty("port", "27017");
        prop.setProperty("dbname", "mydb");
        prop.setProperty("login", "root");
        prop.setProperty("password", "");
        prop.setProperty("table", "dance");

        try {
            // Создаем подключение
            mongoClient = new MongoClient( prop.getProperty("host"), Integer.valueOf(prop.getProperty("port")) );
            // Выбираем БД для дальнейшей работы
            db = mongoClient.getDB(prop.getProperty("dbname"));
            // Входим под созданным логином и паролем
            authenticate = db.authenticate(prop.getProperty("login"), prop.getProperty("password").toCharArray());
            //Выбираем коллекцию/таблицу для дальнейшей работы
            table = db.getCollection(prop.getProperty("table"));
        } catch (UnknownHostException e) {
            System.err.println("Don't connect!");
        }
    }

    //добавление записи
    public void add(Human human){
        BasicDBObject document = new BasicDBObject();

        // указываем поле с объекта User это поле будет записываться в коллекцию/таблицу
        document.put("name", human.getName());
        document.put("x", human.getPoint().x);
        document.put("y", human.getPoint().y);

        // записываем данные в коллекцию/таблицу
        table.insert(document);
    }

    public void droup(){
        table.drop();
    }

    public void getByName(){
        // осуществляем поиск
        DBCursor res = table.find();

        for (DBObject re : res) {
            System.out.print("name: " + re.get("name"));
            System.out.print(" x: " + re.get("x"));
            System.out.println(" y: " + re.get("y"));
        }
    }
}
