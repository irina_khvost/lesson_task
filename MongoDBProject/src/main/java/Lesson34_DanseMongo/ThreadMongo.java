package Lesson34_DanseMongo;

public class ThreadMongo extends Thread {
    MongoConect mongoConect;

    public ThreadMongo() {
        this.mongoConect = new MongoConect();
        start();
    }

    @Override
    public void run() {
        mongoConect.droup();
        for (Human human : Main.humans) {
            mongoConect.add(human);
        }
        mongoConect.getByName();
    }
}
