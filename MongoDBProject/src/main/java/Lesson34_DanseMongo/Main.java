package Lesson34_DanseMongo;
/**
 * 12 графов и графинь танцуют вальс. Каждый такт они перемещаются по танцевальному залу.
 * Нужно создать классы граф и графиня в каждом из которых будет поля “имя” и их координаты (х и у).
 * Каждый такт танца они будут сдвигаться в случайном направлении на единицу.
 * После каждого “шага” нужно записывать их новое положение в bin-файл(удаляя при этом старое).
 * В программе должно быть два режима. Первый это такты танцев, который будет каждый раз спрашивать “еще такт?”
 * до тех пор пока пользователь не скажет остановиться. И второй режим, который считывает bin-файл
 * и показывает на экране координаты и имена каждого из танцующих. Добавить сохранение в БД Mongo
 */

import java.awt.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {
       static volatile ArrayList<Human> humans;
    public static void main(String[] args) throws SQLException {
        // размер поля для танцев
        int n = 25;
        JDBCConect jdbcConect = new JDBCConect();
        jdbcConect.delTable();
        for (Human human : createDanserc(n)) {
            jdbcConect.addVal(human);
        }
        ThreadSQL threadSQL = new ThreadSQL();
        ThreadMongo threadMongo = new ThreadMongo();
        }


    private static ArrayList<Human> createDanserc(int n) {
        ArrayList<Human> dancers = new ArrayList<Human>();
        Random rnd = new Random();
        //количество графов и графинь
        int count = 12;
        for (int i = 0; i < count; i++) {
            dancers.add(new Graph("Count" + i, new Point(rnd.nextInt(n), rnd.nextInt(n))));
            dancers.add(new Countess("Countess" + i, new Point(rnd.nextInt(n), rnd.nextInt(n))));
        }
        return dancers;
    }

    private static void dance(Human human, int n) {
        Random rnd = new Random();
        Step[] step = Step.values();
        int x = (int) human.getPoint().getX();
        int y = (int) human.getPoint().getY();
        boolean flag = true;
        while (flag) {
            int a = rnd.nextInt(step.length);
            switch (a) {
                case 0:
                    human.setPoint(new Point(x, y - 1)); //LEFT
                    break;
                case 1:
                    human.setPoint(new Point(x, y + 1)); //RIGHT
                    break;
                case 2:
                    human.setPoint(new Point(x - 1, y)); //UP
                    break;
                case 3:
                    human.setPoint(new Point(x + 1, y)); //DOWN
                    break;
            }
            if (human.getPoint().getX() <= n || human.getPoint().getY() <= n)
                flag = false;
        }
    }

    private static void writeMongo(ArrayList<Human> humans) {
        MongoConect mongoConect = new MongoConect();
        mongoConect.droup();
        for (Human human : humans) {
            mongoConect.add(human);
        }
    }
}
