package Lesson34_DanseMongo;

import java.sql.SQLException;

public class ThreadSQL extends Thread {
    JDBCConect jdbcConect;

    public ThreadSQL() {
        this.jdbcConect = new JDBCConect();
        start();
        try {
            join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            jdbcConect.getAllData();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
