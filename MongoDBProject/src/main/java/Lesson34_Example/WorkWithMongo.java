package Lesson34_Example;

import com.mongodb.*;
import java.net.UnknownHostException;
import java.util.Properties;

public class WorkWithMongo {

    // это клиент который обеспечит подключение к БД
    private MongoClient mongoClient;

    // В нашем случае, этот класс дает возможность аутентифицироваться в MongoDB
    private DB db;

    // тут мы будем хранить состояние подключения к БД
    private boolean authenticate;

    // И класс который обеспечит возможность работать с коллекциями/таблицами MongoDB
    private DBCollection table;

    Properties prop;

    public WorkWithMongo(){
        prop = new Properties();
        prop.setProperty("host", "localhost");
        prop.setProperty("port", "27017");
        prop.setProperty("dbname", "mydb");
        prop.setProperty("login", "root");
        prop.setProperty("password", "");
        prop.setProperty("table", "users");

        try {
            // Создаем подключение
            mongoClient = new MongoClient( prop.getProperty("host"), Integer.valueOf(prop.getProperty("port")) );
            // Выбираем БД для дальнейшей работы
            db = mongoClient.getDB(prop.getProperty("dbname"));
            // Входим под созданным логином и паролем
            authenticate = db.authenticate(prop.getProperty("login"), prop.getProperty("password").toCharArray());
            //Выбираем коллекцию/таблицу для дальнейшей работы
            table = db.getCollection(prop.getProperty("table"));
        } catch (UnknownHostException e) {
            System.err.println("Don't connect!");
        }
    }

    //добавление записи
    public void add(User user){
        BasicDBObject document = new BasicDBObject();

        // указываем поле с объекта User это поле будет записываться в коллекцию/таблицу
        document.put("name", user.getName());

        // записываем данные в коллекцию/таблицу
        table.insert(document);
    }

    //удаление колекции
    public void delOneColekshen(){
        // записываем данные в коллекцию/таблицу
        table.drop();
        System.out.println("Database delete");
    }


    public User getByName(String name){
        System.out.println("I'll will find in " + prop.getProperty("dbname"));
        System.out.println("In collection " + prop.getProperty("table"));
        System.out.println("By value " + name);
        BasicDBObject query = new BasicDBObject();

        // задаем поле и значение поля по которому будем искать
        query.put("name", name);

        // осуществляем поиск
        DBCursor res = table.find(query);

        for (DBObject re : res) {
            System.out.println(re.get("_id"));
            System.out.println(re.get("name"));
        }

        // Заполняем сущность полученными данными с коллекции
        User user = new User();
       // user.setName(String.valueOf(result.get("name")));
       // user.setId(String.valueOf(result.get("_id")));

        // возвращаем полученного пользователя
        return user;
    }

    // name - это старый логин пользователя newName - это новый логин который мы хотим задать
    public void updateByName(String name, String newName){
        BasicDBObject newData = new BasicDBObject();

        // задаем новый логин
        newData.put("name", newName);

        // указываем обновляемое поле и текущее его значение
        BasicDBObject searchQuery = new BasicDBObject().append("name", name);

        // обновляем запись
        table.update(searchQuery, newData);
    }

    public void deleteByName(String name){
        BasicDBObject query = new BasicDBObject();

        // указываем какую запись будем удалять с коллекции задав поле и его текущее значение
        query.put("name", name);

        // удаляем запись с коллекции/таблицы
        table.remove(query);
    }

    public void deleteByOneName(int id){
        BasicDBObject query = new BasicDBObject();

        // указываем какую запись будем удалять с коллекции задав поле и его текущее значение
        query.put("_id", id);

        // удаляем запись с коллекции/таблицы
        table.remove(query);
    }

}
