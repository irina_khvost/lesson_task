package Lesson35_ChatMongo;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

import static Lesson35_ChatMongo.Server.recvString;
import static Lesson35_ChatMongo.Server.sendString;

public class Client {
    public static void main(String[] args) {
        Socket s = null;
        InputStream is;
        OutputStream os;
        try {
            s = new Socket("localhost", 9999);

            int nPort = s.getLocalPort();
            System.out.println("Local Port: " + nPort);

            is = s.getInputStream();
            os = s.getOutputStream();
            String szStr;
            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.println("pls enter command");
                szStr = scanner.next();
                if (szStr.equals("quit"))
                    break;
                if (szStr.equals("show")) {
                    sendString(os, szStr);

                    do {
                        szStr = recvString(is);
                        System.out.println(szStr);
                    } while (!szStr.equals(""));
                }

                else {
                    sendString(os, szStr);
                }

            }
            is.close();
            os.close();
            s.close();
        } catch (Exception ex) {
            System.out.println(ex.toString());
            System.exit(0);
        }
    }
}
