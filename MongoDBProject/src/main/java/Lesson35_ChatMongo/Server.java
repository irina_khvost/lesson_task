package Lesson35_ChatMongo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        ServerSocket ss = null;
        Socket s = null;
        InputStream is = null;
        OutputStream os = null;
        MongoConect mongoConect = new MongoConect();

        try {
            ss = new ServerSocket(9999);
            // mongoConect.drop();
        } catch (Exception ex) {
            System.out.println(ex.toString());
            System.exit(0);
        }

        int nPort = ss.getLocalPort();

        System.out.println("Local Port: " + nPort);

        while (true) {
            try {
                s = ss.accept();
                is = s.getInputStream();
                os = s.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("We have new connection from : " + s.getInetAddress().toString());


            //s.getInetAddress();
            while (true) {
                String szStr = "";
                try {
                    szStr = recvString(is);
                    System.out.println(szStr);
                    System.out.println("Send: " + szStr);
                    if (szStr.equals("quit")) {
                        sendString(os, "* " + szStr + " *");
                        break;
                    } else if (szStr.equals("show")) {
                        System.out.println("====time to show");
                        mongoConect.getByName(os);
                        os.flush();
                    } else {
                        String name = szStr.split("_")[0];
                        String message = szStr.split("_")[1];
                        mongoConect.add(name, message);
                        os.flush();
                        System.out.println(szStr);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            try {
                is.close();
                os.close();
                s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static void sendString(OutputStream os, String s) throws IOException {
        for (int i = 0; i < s.length(); i++) {
            os.write((byte) s.charAt(i));
        }
        os.write('\n');
        os.flush();
    }

    static String recvString(InputStream is) {
        String szBuf = "";
        int ch = 0;
        try {
            ch = is.read();
            while (ch >= 0 && ch != '\n') {
                szBuf += (char) ch;
                ch = is.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
//            return szBuf;
        }
        return szBuf;
    }
}
