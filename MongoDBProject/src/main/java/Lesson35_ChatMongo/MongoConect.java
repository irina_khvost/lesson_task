package Lesson35_ChatMongo;

import java.io.IOException;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.util.Properties;

import com.mongodb.*;

public class MongoConect {
    // это клиент который обеспечит подключение к БД
    private MongoClient mongoClient;

    // В нашем случае, этот класс дает возможность аутентифицироваться в MongoDB
    private DB db;

    // тут мы будем хранить состояние подключения к БД
    private boolean authenticate;

    // И класс который обеспечит возможность работать с коллекциями/таблицами MongoDB
    private DBCollection table;

    Properties prop;

    public MongoConect() {
        prop = new Properties();
        prop.setProperty("host", "localhost");
        prop.setProperty("port", "27017");
        prop.setProperty("dbname", "mydb");
        prop.setProperty("login", "root");
        prop.setProperty("password", "");
        prop.setProperty("table", "messages");

        try {
            // Создаем подключение
            mongoClient = new MongoClient(prop.getProperty("host"), Integer.valueOf(prop.getProperty("port")));
            // Выбираем БД для дальнейшей работы
            db = mongoClient.getDB(prop.getProperty("dbname"));
            // Входим под созданным логином и паролем
            authenticate = db.authenticate(prop.getProperty("login"), prop.getProperty("password").toCharArray());
            //Выбираем коллекцию/таблицу для дальнейшей работы
            table = db.getCollection(prop.getProperty("table"));
        } catch (UnknownHostException e) {
            System.err.println("Don't connect!");
        }
    }
    public void drop() {
        BasicDBObject document = new BasicDBObject();
        table.drop();
    }


    //добавление записи
    public void add(String name, String message) {
        BasicDBObject document = new BasicDBObject();

        // указываем поле с объекта User это поле будет записываться в коллекцию/таблицу
        document.put("name", name);
        document.put("message", message);

        // записываем данные в коллекцию/таблицу
        table.insert(document);
    }

    public void getByName(OutputStream os) throws IOException {
        // осуществляем поиск
        DBCursor res = table.find();

        for (DBObject re : res) {
            System.out.println("name: " + re.get("name"));
            System.out.println("message: " + re.get("message"));
            os.write(("name: " + re.get("name")).getBytes());
            os.write(("  message: " + re.get("message")).getBytes());
            os.write('\n');
        }



    }
}

